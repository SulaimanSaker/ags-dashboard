export const CategoryEnum = {
  Solar_Panel: "Solar Panel",
  Battery: "Battery",
  Connector: "Connector",
  Charger: "Charger",
  Inverter: "Inverter",
};
