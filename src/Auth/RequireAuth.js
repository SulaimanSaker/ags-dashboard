import { useLocation, Navigate, Outlet } from "react-router-dom";

const RequireAuth = () => {
  const location = useLocation();

  let isLogin = localStorage.getItem("username") && localStorage.getItem("password") ? true : false;
  const content = isLogin ? <Outlet /> : <Navigate to="/login" state={{ from: location }} replace />;

  return content;
};
export default RequireAuth;
