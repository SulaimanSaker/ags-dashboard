// import "./Global/Global.scss";
import AOS from "aos";
import Home from "./components/Home/Home";
import Website from "./components/Website";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  useNavigate,
  Redirect,
  useLocation,
  redirect,
} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Main from "./components/Main/Main";
import About from "./components/Main/About/About";
import Services from "./components/Services/Services";
import Blogs from "./components/Blogs/Blogs";
import Features from "./components/Main/Features/Features";
import Quote from "./components/Quote/Quote";
import TeamMember from "./components/Main/TeamMember/Team";
import Testimonial from "./components/Testimonial/Testimonial";
import Contact from "./components/Contact/Contact";
import SubLayout from "./components/SubLayout/WrapperLayout";
import WrapperLayout from "./components/SubLayout/WrapperLayout";
import ProductsWrapper from "./components/Products/ProductsWrapper";
import ProductsWrapperAdmin from "./components/AdminLayout/Home/Products/ProductsWrapper";
import AllBlogs from "./components/AllBlogs/AllBlogs";
import BlogDetail from "./components/BlogDetail/BlogDetail";
import ProductDetail from "./components/Products/Products/Product/ProductDetail/ProductDetail";
import Login from "./components/Login/Login";
import AdminLayout from "./components/AdminLayout/AdminLayout";
import Product from "./components/AdminLayout/Home/Products/Products/Product/Product";
import ProductDetailAdmin from "./components/AdminLayout/Home/Products/Products/Product/ProductDetail/ProductDetail";
// import "/node_modules/primeflex/primeflex.css";
//theme
import "primereact/resources/themes/lara-light-indigo/theme.css";
//core
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "aos/dist/aos.css";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import BlogWrapper from "./components/AdminLayout/Home/Blogs/BlogWrapper";
import UpdateBlog from "./components/AdminLayout/Home/Blogs/UpdateBlog/UpdateBlog";
import NewBlog from "./components/AdminLayout/Home/Blogs/NewBlog/NewBlog";
import ProtectedRoute from "./utils/ProtectedRoute";
import Messages from "./components/AdminLayout/Home/Messages/Messages";
import AllTeam from "./components/Main/AllTeam/AllTeam";
import { ToastContainer } from "react-toastify";
import { useState } from "react";
import RequireAuth from "./Auth/RequireAuth";
import NotFound from "./components/NotFound/NotFound";
import CartItems from "./components/CartItems/CartItems";
import ReactGA from "react-ga";
import TagManager from "react-gtm-module";
import Quotes from "./components/AdminLayout/Home/Quotes/Quotes";
import ServiceWrapper from "./components/AdminLayout/Home/Services/BlogWrapper";
import UpdateService from "./components/AdminLayout/Home/Services/UpdateBlog/UpdateBlog";
import NewService from "./components/AdminLayout/Home/Services/NewBlog/NewBlog";

const hasToken = () => {
  // Check if a token exists in local storage or any other storage mechanism
  const token = localStorage.getItem("username");
  return !!token; // Return true if a token exists, false otherwise
};
const RedirectionToLogin = () => {
  let navigate = useNavigate();
  const handleFormSubmit = () => {
    navigate("/about", { replace: true });
  };

  return <div>{navigate("login")}</div>;
};

ReactGA.initialize("G-BMXEF2Q1MG", { debug: true });
const tagManagerArgs = {
  gtmId: "GTM-N2SVRTFC",
};
TagManager.initialize(tagManagerArgs);

function App() {
  AOS.init();

  return (
    <>
      <ToastContainer />
      <BrowserRouter>
        <Routes>
          <Route element={<RequireAuth />}>
            <Route path="admin-ags-793" element={<AdminLayout />}>
              <Route index path="products" element={<ProductsWrapperAdmin />} />
              <Route path="product-detail" element={<ProductDetailAdmin />} />
              <Route path="blogs" element={<BlogWrapper />} />
              <Route path="new-blog" element={<NewBlog />} />
              <Route path="edit-blog" element={<UpdateBlog />} />
              <Route path="messages" element={<Messages />} />
              <Route path="quotes" element={<Quotes />} />

              <Route path="services" element={<ServiceWrapper />} />
              <Route path="new-service" element={<NewService />} />
              <Route path="edit-service" element={<UpdateService />} />
            </Route>
          </Route>
          <Route path="product-detail/:id" element={<ProductDetail />} />
          <Route path="products" element={<ProductsWrapper />} />
          <Route path="/" element={<Layout />}>
            <Route index element={<Main />} />
            <Route path="blogs" element={<BlogDetail />} />
            <Route path="blogs/:id" element={<BlogDetail />} />
            <Route path="blog-detail" element={<BlogDetail />} />
            <Route path="login" element={<Login />} />
            <Route path="cart" element={<CartItems />} />
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
