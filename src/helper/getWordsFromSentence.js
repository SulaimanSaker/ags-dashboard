export function getWordsFromSentence(str, numOfWords) {
  return str ? str.split(/\s+/).slice(0, numOfWords).join(" ") : "";
}
