export function countWords(str) {
  const arr = str.split(" ");
  return arr.filter((word) => word !== "").length;
}
export function countCharacters(str) {
  return str ? str.length : 0;
}
