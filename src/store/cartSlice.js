import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  products: [],
};
export const cartsSlice = createSlice({
  name: "cart",
  initialState: initialState,
  reducers: {
    addProductToCart: (state, action) => {
      state.products.push(action.payload);
    },
    emptyCart: (state, action) => {
      state.products = [];
    },
    removeProductToCart: (state, action) => {
      var index = state.products.findIndex((p) => p._id === action.payload._id);
      if (index !== -1) {
        state.products.splice(index, 1);
      }
      // state.products.push(action.payload);
    },
  },
});

export const { addProductToCart, removeProductToCart, emptyCart } = cartsSlice.actions;

export default cartsSlice.reducer;
