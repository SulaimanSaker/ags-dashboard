import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allProducts: [],
};
export const productsSlice = createSlice({
  name: "product",
  initialState: initialState,
  reducers: {
    fetchAll: (state, action) => {
      //   const { data } = action.payload;
      state.products = action.payload;
    },
  },
});

export const { fetchAll } = productsSlice.actions;

export default productsSlice.reducer;
