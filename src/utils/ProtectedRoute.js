import React from "react";
import { useSelector } from "react-redux";
import { Navigate, useLocation, useNavigate } from "react-router-dom";

const ProtectedRoute = () => {
  const isAuthenticated = () => {
    return localStorage.getItem("username") && localStorage.getItem("password") !== null;
  };
  const isAuthorized = () => {
    if (isAuthenticated()) {
      // User is authenticated, allow access to the dashboard
      return true;
    } else {
      // User is not authenticated, navigate to the login page
      return false;
    }
  };
  return isAuthorized();
};

export default ProtectedRoute;
