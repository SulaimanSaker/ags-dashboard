import * as api from "../api/index.js";
import { fetchAll } from "../store/productsSlice.js";

export const getProducts = (page, pageSize) => async (dispatch) => {
  try {
    const data = await api.fetchProducts(page, pageSize);
    dispatch(fetchAll(data.data));
  } catch (error) {
    console.log(error);
  }
};
export const downloadDatasheet = async (id, fileName) => {
  try {
    fetch(api.BASE_URL + "/download/" + id).then((response) => {
      response.blob().then((blob) => {
        const fileURL = window.URL.createObjectURL(blob);
        window.open(fileURL, "_blank");
        let alink = document.createElement("a");
        alink.href = fileURL;
        alink.download = `${fileName.trim()}.pdf`;
        alink.click();
      });
    });
  } catch (error) {
    console.log(error);
  }
};
