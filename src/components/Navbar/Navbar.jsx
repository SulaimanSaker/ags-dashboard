import React, { useState } from "react";
import "./Navbar.scss";
import { Link } from "react-router-dom";
import { isMobile } from "react-device-detect";
import { useSelector } from "react-redux";
const Navbar = () => {
  let productsCart = useSelector((state) => state.cart.products);
  const [activeLabel, setActiveLabel] = useState("home");
  const [isOpen, setIsOpen] = useState(true);
  const jumpToBlock = (id) => {
    if (isMobile) {
      setActiveLabel(id);
      const toggle_id = document.getElementById("navbarCollapse");
      toggle_id.classList.remove("show");
      let isOpenBuff = true;
      setIsOpen(isOpenBuff);
      setTimeout(() => {
        document.getElementById(id).scrollIntoView({ behavior: "smooth" });
      }, 500);
    } else {
      setActiveLabel(id);
      setTimeout(() => {
        document.getElementById(id).scrollIntoView({ behavior: "smooth" });
      }, 500);
    }
  };
  const jumpToRoute = (id) => {
    if (isMobile) {
      setActiveLabel(id);
      const toggle_id = document.getElementById("navbarCollapse");
      toggle_id.classList.remove("show");
      let isOpenBuff = true;
      setIsOpen(isOpenBuff);
    }
  };
  const isToggle = () => {
    const toggle_id = document.getElementById("toggle_id");
    setIsOpen(toggle_id.classList.contains("collapsed"));
    return isOpen;
  };
  return (
    <div>
      <div className="container-fluid position-relative p-0" id="navbar_id">
        <nav className="navbar navbar-expand-lg navbar-dark  py-3 py-lg-0" id="logo-bg">
          <Link to="/" className="navbar-brand p-0">
            <h1 className="m-0 ">
              <span className="icon-logo">AGS</span>{" "}
            </h1>
          </Link>
          <button className="navbar-toggler" onClick={() => isToggle()} type="button" id="toggle_id" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <i className={`bx ${isOpen ? "bx-menu-alt-right" : "bx-x"}`}></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <div className="navbar-nav ms-auto py-0">
              <Link
                id="nav-item"
                to="/products"
                data-wow-delay="1.3s"
                onClick={() => {
                  setActiveLabel("products");
                  jumpToRoute("products");
                }}
                className={`nav-item nav-link   ${activeLabel === "products" ? "active" : ""}`}
              >
                Products
              </Link>
              <Link id="services-item" to="/" onClick={() => jumpToBlock("services_id")} className={`nav-item nav-link   ${activeLabel === "services_id" ? "active" : ""}`}>
                Services
              </Link>
              <Link id="team-item" to="/" onClick={() => jumpToBlock("team")} className={`nav-item nav-link   ${activeLabel === "team" ? "active" : ""}`}>
                Team
              </Link>
              <Link to="/blogs" id="blogs-id" onClick={() => jumpToBlock("blogDetail")} className={`nav-item nav-link   ${activeLabel === "blogs_id" ? "active" : ""}`}>
                Blogs
              </Link>
              <Link id="contact-item" to="/" onClick={() => jumpToBlock("contact")} className={`nav-item nav-link   ${activeLabel === "contact" ? "active" : ""}`}>
                Contact
              </Link>
            </div>
            <Link
              to="/cart"
              onClick={() => {
                jumpToRoute("cart_id");
              }}
              className=" text-white py-1 px-2 ms-3"
            >
              <div className="cart-icon ">
                <img src="../img/Icons/shopping-cart.png" alt="shopping-cart.png" id="cart" />
                <span class="badge badge-danger">{productsCart?.length}</span>
              </div>
            </Link>
            <Link id="btn-login" to="/login" className="btn btn-login btn-primary text-white py-2 px-4 ms-3">
              Login
            </Link>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
