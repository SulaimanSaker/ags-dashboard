import React from "react";
import "./Topbar.scss";
import { SocialMedia } from "../../Data/Data";
const Topbar = () => {
  return (
    <div className="container-bar">
      <div className="container-fluid bg-dark px-5 d-none d-lg-block">
        <div className="row gx-0">
          <div className="col-lg-8 text-center text-lg-start mb-2 mb-lg-0">
            <div className="d-inline-flex align-items-center bar">
              <a className="bar-item me-3 text-light wow rotateInDownLeft" data-wow-delay=".2s" href="https://goo.gl/maps/D61uNbU9abQZ6CPZ6" target="_blank" rel="noreferrer">
                <i className="fa fa-map-marker-alt me-2"></i>11 Street 25 - Naif - Dubai- AUE
              </a>
              <a className="bar-item me-3 text-light wow rotateInDownLeft" href="tel:+971542899793" data-wow-delay=".6s" rel="noreferrer">
                <i className="fa fa-phone-alt me-2"></i>+971 54 289 9793
              </a>
              <a className=" bar-item text-light wow rotateInDownLeft" href="mailTo:info@ags.ac" data-wow-delay=".9s" target="_blank" rel="noreferrer">
                <i className="fa fa-envelope-open me-2"></i>info@ags.ac
              </a>
            </div>
          </div>
          <div className="col-lg-4 text-center  text-lg-end">
            <div className="d-inline-flex align-items-center">
              {SocialMedia.map((item, index) => (
                <a
                  key={index}
                  data-wow-delay={item.delay}
                  className="btn btn-sm btn-outline-light btn-sm-square  wow rotateInDownRight rounded-circle me-2"
                  target="_blank"
                  href={item.link}
                  rel="noreferrer"
                  aria-label={item.altDescription}
                >
                  <i className={`fab ${item.icon} fw-normal`}></i>
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Topbar;
