import { Toast } from "primereact/toast";
import React, { useRef } from "react";

const ToasterCustom = () => {
  const toast = useRef(null);

  const show = () => {
    toast.current.show({ severity: "success", summary: "Form Submitted", detail: "" });
  };
  return (
    <div>
      <Toast ref={toast} />
    </div>
  );
};

export default ToasterCustom;
