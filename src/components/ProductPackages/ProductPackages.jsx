import React, { useEffect, useRef } from "react";
import "./ProductPackages.scss";
import OwlCarousel from "react-owl-carousel";
import { useState } from "react";
import { Packages } from "../../Data/Data";
import { Dialog } from "primereact/dialog";
const ProductPackages = () => {
  const [visible, setVisible] = useState(false);
  const [position, setPosition] = useState("center");
  const [orderRequest, setOrderRequest] = useState({
    userName: "",
    email: "",
    address: "",
    phone: "",
    date: new Date(),
  });
  const onChangeHandler = (e) => {
    const { name, value } = e.target;
    setOrderRequest((prev) => ({ ...prev, [name]: value }));
  };
  const optionsPackage = {
    loop: true,
    margin: 20,
    nav: false,
    dots: false,
    center: true,
    autoplaySpeed: 8000,
    autoplayTimeout: 8000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 2,
      },
    },
  };
  const show = (position) => {
    setPosition(position);
    setVisible(true);
  };
  const sendOrder = () => {
    setVisible(false);
    setOrderRequest({
      userName: "",
      email: "",
      address: "",
      phone: "",
    });
  };
  return (
    <>
      {Packages?.length > 0 && (
        <div className="package-parent">
          <div className="package-products ">
            <div className="container-fluid">
              <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
                <h5 className="fw-bold text-primary text-uppercase">Ongoing Packages</h5>
              </div>
              <OwlCarousel {...optionsPackage} className="owl-theme ">
                {Packages.map((packageItem, index) => (
                  <div key={index} class="package-slider__item swiper-slide" onClick={() => show("top")}>
                    <div class="package-slider__img">
                      <img src={packageItem.image} alt="" />
                    </div>
                    <div class="blog-slider__content">
                      <div class="blog-slider__title">{packageItem.name}</div>
                      <div class="blog-slider__text">
                        <ul>
                          {packageItem.itemsDescription.map((item, indexPackage) => (
                            <li key={indexPackage}>
                              <div className="item-package">
                                <img src="../img/Icons/colorkit.svg" alt="icon-check" />
                                <span> {item.name}</span>
                              </div>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
          <Dialog header="Order" visible={visible} position={position} onHide={() => setVisible(false)} draggable={false} resizable={false}>
            <div>
              <div className="form-order">
                <form>
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label htmlFor="username_id">User Name</label>
                        <input type="text" className="form-control" name="userName" value={orderRequest.userName} onChange={onChangeHandler} />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label htmlFor="username_id">Email</label>
                        <input type="email" className="form-control" name="email" value={orderRequest.email} onChange={onChangeHandler} />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label htmlFor="username_id">Phone</label>
                        <input type="email" className="form-control" name="address" value={orderRequest.address} onChange={onChangeHandler} />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label htmlFor="username_id">Address</label>
                        <input type="email" className="form-control" name="phone" value={orderRequest.phone} onChange={onChangeHandler} />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="footer-Dialog ">
                <button className="ags-btn-main-outline" onClick={() => setVisible(false)}>
                  Cancel
                </button>
                <button className="ags-btn-main-fill" onClick={() => sendOrder()}>
                  Send
                </button>
              </div>
            </div>
          </Dialog>
        </div>
      )}
    </>
  );
};

export default ProductPackages;
