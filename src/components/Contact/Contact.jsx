import React from "react";
import "./Contact.scss";
import { useState } from "react";
import { addMessage } from "../../api";
const Contact = () => {
  const [contactModel, setContactModel] = useState({
    username: "",
    email: "",
    subject: "",
    message: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setContactModel((prev) => ({ ...prev, [name]: value }));
  };
  const sendMessage = () => {
    addMessage(contactModel)
      .then((response) => {
        setContactModel({
          username: "",
          email: "",
          subject: "",
          message: "",
        });
      })
      .catch((err) => {});
  };
  return (
    <div id="contact">
      <div className="container-fluid py-5 wow fadeInUp contact" data-wow-delay="0.1s">
        <div className="container py-2">
          <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
            <h5 className="fw-bold text-primary text-uppercase">Contact Us</h5>
            <h1 className="mb-0">If You Have Any Query, Feel Free To Contact Us</h1>
          </div>
          <div className="row g-5 mb-5">
            <div className="col-lg-4">
              <div className="contact-item d-flex align-items-center wow fadeIn" data-wow-delay="0.1s">
                <div className="bg-primary d-flex align-items-center justify-content-center rounded">
                  <i className="fa fa-phone-alt text-white"></i>
                </div>
                <div className="ps-4">
                  <h5 className="mb-2">Call to ask any question</h5>
                  <h5 className="text-primary mb-0">
                    <a className="bar-item me-3 text-primary" href="tel:+971542899793" rel="noreferrer">
                      <i className="fa fa-phone-alt me-2"></i>+971 54 289 9793
                    </a>
                  </h5>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="contact-item d-flex align-items-center wow fadeIn" data-wow-delay="0.4s">
                <div className="bg-primary d-flex align-items-center justify-content-center rounded">
                  <i className="fa fa-envelope-open text-white"></i>
                </div>
                <div className="ps-4">
                  <h5 className="mb-2">Email to get free quote</h5>
                  <h5 className="text-primary mb-0">
                    <a className=" bar-item text-primary" href="mailTo:info@ags.ac" target="_blank" rel="noreferrer">
                      <i className="fa fa-envelope-open me-2"></i>info@ags.ac
                    </a>
                  </h5>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="contact-item d-flex align-items-center wow fadeIn" data-wow-delay="0.8s">
                <div className="bg-primary d-flex align-items-center justify-content-center rounded">
                  <a>
                    <i className="fa fa-map-marker-alt text-white"></i>
                  </a>
                </div>
                <div className="ps-4">
                  <h5 className="mb-2">Visit our office</h5>
                  <h5 className="text-primary mb-0">
                    <a className="bar-item me-3 text-primary" href="https://goo.gl/maps/D61uNbU9abQZ6CPZ6" target="_blank" rel="noreferrer">
                      <i className="fa fa-map-marker-alt me-2"></i>11 Street 25 - Naif - Dubai
                    </a>
                  </h5>
                </div>
              </div>
            </div>
          </div>
          <div className="row g-5">
            <div className="col-lg-6 wow slideInUp" data-wow-delay="0.3s">
              <form>
                <div className="row g-3 contact-form">
                  <div className="col-md-6">
                    <input type="text" className="input-item form-control border-0 bg-light px-4" name="username" onChange={handleChange} placeholder="Your Name" />
                  </div>
                  <div className="col-md-6">
                    <input type="email" className="input-item form-control border-0 bg-light px-4" name="email" onChange={handleChange} placeholder="Your Email" />
                  </div>
                  <div className="col-12">
                    <input type="text" className="input-item form-control border-0 bg-light px-4" name="subject" onChange={handleChange} placeholder="Subject" />
                  </div>
                  <div className="col-12">
                    <textarea className="form-control border-0 bg-light px-4 py-3" rows="4" name="message" onChange={handleChange} placeholder="Message"></textarea>
                  </div>
                  <div className="col-12">
                    <button type="button" className="btn btn-primary w-100 py-3" onClick={() => sendMessage()}>
                      Send Message
                    </button>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-lg-6 col-sm-12 wow slideInUp" data-wow-delay="0.6s">
              <div className="map">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d230900.0165795661!2d55.45781032043147!3d25.276883402810352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f432abac0d869%3A0x9e939121d4d15d14!2sAGS%20international%20General%20Trading!5e0!3m2!1sar!2sae!4v1694430269708!5m2!1sar!2sae"
                  width="100%"
                  height="350"
                  allowFullScreen=""
                  loading="lazy"
                  referrerPolicy="no-referrer-when-downgrade"
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
