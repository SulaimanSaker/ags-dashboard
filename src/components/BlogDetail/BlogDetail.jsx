/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import "./BlogDetail.scss";
import Heading from "../Heading/Heading";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { getWordsFromSentence } from "../../helper/getWordsFromSentence";
import { getBlogById, getBlogs } from "../../api/index.js";
import { Grid } from "react-loader-spinner";
const BlogDetail = () => {
  const navigate = useNavigate();
  const [blogs, setBlogs] = useState([]);
  const [isMore, setIsMore] = useState(true);
  const [blogModel, setBlogModel] = useState({});
  let [loading, setLoading] = useState(false);
  let { id } = useParams();
  useEffect(() => {
    setLoading(true);
    getBlogs()
      .then((response) => {
        setBlogs(response.data);
        console.log("id", id);
        if (id) {
          getBlogById(id)
            .then((response) => {
              setBlogModel(response.data);
            })
            .catch((error) => {});
        } else {
          setBlogModel(response.data[0]);
        }

        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);
  const goToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };
  const getDate = (originalDateString) => {
    // Original date string
    // Parse the original date string
    const originalDate = new Date(originalDateString);
    // Extract the year, month, and day from the original date
    const year = originalDate.getUTCFullYear(); // 2023
    const month = originalDate.getUTCMonth() + 1; // Month is zero-based, so add 1 to get the correct month
    const day = originalDate.getUTCDate(); // 27
    // Format the date string as "01 Jan, 2045"
    const formattedDate = `${day.toString().padStart(2, "0")} ${getMonthAbbreviation(month)}, ${year}`;
    return formattedDate;
  };
  // Helper function to get the abbreviated month name
  function getMonthAbbreviation(month) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return months[month - 1];
  }
  const setBlogHandle = (blog) => {
    setBlogModel(blog);
    console.log("/blogs", blog._id);
    navigate(`/blogs/${blog._id}`, { replace: true });
  };
  return (
    <div id="blogDetail">
      <Heading headingInfo={{ heading: "Blogs", title: "Home", subTitle: "Blog_1" }} />
      {loading && (
        <div className="box-spinner">
          <Grid height="80" width="80" color="#cf0a2c" ariaLabel="grid-loading" radius="12.5" wrapperStyle={{}} wrapperClass="" visible={true} />
        </div>
      )}
      {blogs && !loading && (
        <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
          <div className="container py-2">
            <div className="row g-5">
              <div className="col-lg-8">
                <div className="mb-5">
                  <img className="img-fluid img-blog w-100 rounded mb-3" src={blogModel?.file} alt="" />
                  <div className="d-flex mb-3">
                    <small className="me-3">
                      <i className="far fa-user text-primary me-2"></i>Mohammad Chhada
                    </small>
                    <small>
                      <i className="far fa-calendar-alt text-primary me-2"></i>
                      {getDate(blogModel?.date)}
                    </small>
                  </div>
                  <h3 className="mb-4">{blogModel?.title}</h3>
                  <p>
                    {getWordsFromSentence(blogModel?.description, isMore ? 150 : 1000)}{" "}
                    <span className="more-text" onClick={() => setIsMore((prev) => !prev)}>
                      {setIsMore && <span>{`${isMore ? "read more...." : "read less"}`}</span>}
                    </span>
                  </p>
                </div>
                <div className="mb-5"></div>
              </div>
              <div className="col-lg-4">
                <div className="mb-5 wow slideInUp" data-wow-delay="0.1s">
                  <div className="section-title section-title-sm position-relative pb-3 mb-4">
                    <h5 className="mb-0">Recent Post</h5>
                  </div>
                  {blogs.slice(1, 8).map((blog, index) => (
                    <div index={index} className="d-flex recent-blog-item  rounded overflow-hidden mb-3" onClick={() => setBlogHandle(blog)}>
                      <img className="img-fluid" src={blog.file} style={{ width: "100px", height: "100px", objectFit: "cover" }} alt="" />
                      <a className="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">{getWordsFromSentence(blog.title, 6)}</a>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="container-fluid others-blogs  wow fadeInUp" data-wow-delay="0.1s" id="blogs_id">
              <div className="blogs-wrapper">
                <div className="row g-2">
                  <div className="col-lg-12">
                    <div className="row g-4">
                      {blogs.slice(6, blogs.length).map((blog, index) => (
                        <div key={index} className="col-md-3 wow slideInUp" data-wow-delay="0.1s" onClick={() => setBlogModel(blog)}>
                          <div className="blog-item bg-light rounded overflow-hidden">
                            <div className="blog-img position-relative overflow-hidden">
                              <img className="img-fluid" src={blog.file} alt="" />
                            </div>
                            <div className="blog-info p-3">
                              <div className="d-flex mb-3">
                                <small>
                                  <i className="far fa-calendar-alt text-primary me-2"></i>
                                  {getDate(blog.date)}
                                </small>
                              </div>
                              <h4 className="mb-2">{getWordsFromSentence(blog.title, 3)}</h4>
                              <p className="mb-2">{getWordsFromSentence(blog.description, 10)}....</p>
                              <a
                                className="read-more"
                                onClick={() => {
                                  setBlogHandle(blog);
                                  goToTop();
                                }}
                              >
                                Read More <i className="bi bi-arrow-right"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>{" "}
        </div>
      )}
    </div>
  );
};

export default BlogDetail;
