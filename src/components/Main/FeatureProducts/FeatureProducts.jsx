import React from "react";
import "./FeatureProducts.scss";
import OwlCarousel from "react-owl-carousel";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const FeatureProducts = () => {
  const navigate = useNavigate();
  let buffProducts = useSelector((state) => (state.products ? state.products : []));
  let allProducts = buffProducts?.products?.products.slice(0, 11);
  const optionsFirst = {
    loop: true,
    margin: 6,
    nav: false,
    dots: false,
    center: false,
    autoplaySpeed: 5000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 5,
      },
    },
  };
  return (
    <>
      {allProducts?.length > 0 && (
        <div className="feature-products-wrapper">
          <div className="container-fluid product-item-img-container  wow fadeInUp" data-wow-delay="0.1s" id="team">
            <div className="container ">
              <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
                <h1 className="heading-feature fw-bold text-primary text-uppercase">Feature Products</h1>
              </div>
              <div className="row g-5">
                <OwlCarousel {...optionsFirst} className="owl-theme ">
                  {allProducts?.map((item, index) => (
                    <div key={index} className="col-lg-12 item-member wow fadeInLeft border-rounder" data-wow-delay="0.3s">
                      <div className="product-item  rounded overflow-hidden" onClick={() => navigate("/products")}>
                        <div className="product-item-img position-relative overflow-hidden">
                          <img className="img-fluid img-member w-100" src={item.image[0]} alt="" />
                        </div>
                        <div className="text-center py-4">
                          <h4 className="text-primary">
                            {item.brand} {item.code}
                          </h4>
                          <p className="text-uppercase m-0"> {item.capacity}</p>
                        </div>
                      </div>
                    </div>
                  ))}
                </OwlCarousel>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default FeatureProducts;
