import React from "react";
import Suppliers from "./Suppliers/Suppliers";
import Facts from "../Facts/Facts";
import Services from "../Services/Services";
import TeamMember from "./TeamMember/Team";
import Contact from "../Contact/Contact";
import ProductPackages from "../ProductPackages/ProductPackages";

import { useEffect } from "react";
import { useDispatch } from "react-redux";
import NewHeader from "../NewHeader/NewHeader";
import { isMobile } from "react-device-detect";
const whiteColor = "#fff";
const darkColor = "#091E3E";
const borderColor = "#091E3E";
function Main() {
  const dispatch = useDispatch();
  useEffect(() => {
    if (isMobile) {
      document.getElementById("nav-item").style.color = darkColor;
      document.getElementById("contact-item").style.color = darkColor;
      document.getElementById("services-item").style.color = darkColor;
      document.getElementById("contact-item").style.color = darkColor;
      document.getElementById("team-item").style.color = darkColor;
      document.getElementById("blogs-id").style.color = darkColor;
      document.getElementById("cart").style.filter = "invert(0)";
      document.getElementById("cart").style.marginLeft = "auto";
      document.getElementById("cart").style.width = "10%";
      console.log("isMobile");
    } else {
      document.getElementById("nav-item").style.color = whiteColor;
      document.getElementById("contact-item").style.color = whiteColor;
      document.getElementById("services-item").style.color = whiteColor;
      document.getElementById("contact-item").style.color = whiteColor;
      document.getElementById("team-item").style.color = whiteColor;
      document.getElementById("blogs-id").style.color = whiteColor;
      document.getElementById("btn-login").style.borderColor = borderColor;
      document.getElementById("cart").style.filter = "invert(1)";
      document.getElementById("cart").style.marginLeft = "0";
      document.getElementById("cart").style.width = "40%";
      console.log("desktop");
    }
  }, [dispatch]);
  return (
    <>
      <main id="main">
        <NewHeader />
        <Facts />
        <Suppliers />
        <Services />
        {/* <ProductPackages /> */}
        <TeamMember />
        <Contact />
      </main>
    </>
  );
}

export default Main;
