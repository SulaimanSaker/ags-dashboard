import React from "react";
import "./About.scss";
const About = () => {
  const onGetQuoteClick = () => {
    const phoneNumber = "971565527684";
    const message = "Hello AGS, I'm looking for more information about products";
    const whatsappUrl = `https://wa.me/${phoneNumber}?text=${message}`;
    window.open(whatsappUrl);
  };
  return (
    <div className="about" id="about">
      <div className="container-fluid  wow rotateInDownLeft" data-wow-delay="0.1s">
        <div className="container ">
          <div className="row g-5">
            <div className="col-lg-7">
              <div className="section-title position-relative pb-3 ">
                <h5 className="fw-bold text-primary text-uppercase">About Us</h5>
                <h1 className="mb-0">The Best Power Solution With 10 Years of Experience</h1>
              </div>
              <p className="about-text mb-4">
                AGS International Power Solution is committed to revolutionizing global energy through innovative, sustainable solutions. With a rich history of 10+ years, we've
                become leaders in power generation and management. Our mission: provide reliable, eco-friendly power for thriving communities and a sustainable future.
              </p>
              <div className="row about-items g-0 mb-3">
                <div className="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                  <h5 className="mb-3">
                    <i className="fa fa-check text-primary me-3"></i>Award Winning
                  </h5>
                  <h5 className="mb-3">
                    <i className="fa fa-check text-primary me-3"></i>Professional Staff
                  </h5>
                </div>
                <div className="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                  <h5 className="mb-3">
                    <i className="fa fa-check text-primary me-3"></i>24/7 Support
                  </h5>
                  <h5 className="mb-3">
                    <i className="fa fa-check text-primary me-3"></i>Fair Prices
                  </h5>
                </div>
              </div>
              <div className="d-flex call-box align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                <div className="call-icon bg-primary d-flex align-items-center justify-content-center rounded">
                  <i className="fa fa-phone-alt text-white"></i>
                </div>
                <div className="ps-4">
                  <h5 className="mb-2">Call to ask any question</h5>
                  <h4 className="text-danger mb-0">
                    <a className="text-danger" href="tel:+971452550887">
                      +971 4 5255 0887
                    </a>
                  </h4>
                </div>
              </div>
              <a onClick={() => onGetQuoteClick()} className="btn btn-primary  wow rotateInUpLeft btn-quote" data-wow-delay="0.8s">
                Request A Quote
              </a>
            </div>
            <div className="about-img-box col-lg-5">
              <div className="position-relative img-wrapper">
                <img className="position-absolute w-100 h-100 rounded wow rotateInDownRight border-rounder" data-wow-delay="0.9s" src="img/about_ags.jpg" alt="img-about" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
