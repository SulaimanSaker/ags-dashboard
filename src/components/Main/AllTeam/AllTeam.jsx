import React from "react";
import Heading from "../../Heading/Heading";
import { AllMembers } from "../../../Data/Data";
import "./AllTeam.scss";
const AllTeam = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
  return (
    <div className="all-team" id="team_id">
      <Heading headingInfo={{ heading: "Team", title: "Home", subTitle: "Team" }} />
      <div className="container-fluid">
        <div className="team-wrapper">
          <div className="container-fluid team-container  wow fadeInUp" data-wow-delay="0.1s" id="team">
            <div className="member-wrapper">
              {AllMembers.map((member, index) => (
                <div key={index} className="member-box  wow fadeInLeft border-rounder" data-wow-delay="0.3s">
                  <div className="team-item bg-light rounded overflow-hidden">
                    <div className="team-img position-relative overflow-hidden">
                      <img className="img-fluid img-member w-100" src={member.imageSrc} alt="" />
                      <div className="team-social">
                        {member.SocialMedia.map((item, indexS) => (
                          <div key={indexS}>
                            {item.name === "Email" ? (
                              <a className="btn btn-lg btn-primary btn-lg-square rounded" href={`mailto:${item.link}`} target="_blank" rel="noreferrer">
                                <i className={`${item.icon}  fw-normal`}></i>
                              </a>
                            ) : (
                              <a className="btn btn-lg btn-primary btn-lg-square rounded" href={item.link} target="_blank" rel="noreferrer">
                                <i className={`${item.icon}  fw-normal`}></i>
                              </a>
                            )}
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="text-center member-footer">
                      <h4 className="text-primary">{member.name}</h4>
                      <p className="text-uppercase m-0">{member.position}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllTeam;
