import React from "react";
import "./Features.scss";
const Features = () => {
  return (
    <div className="container-fluid py-5 wow fadeInUp features" data-wow-delay="0.1s" id="features">
      <div className="container ">
        <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
          <h5 className="fw-bold text-primary text-uppercase">Why Choose Us</h5>
          <h1 className="mb-0">We Are Here to Grow Your Business Exponentially</h1>
        </div>
        <div className="row g-5">
          <div className="col-lg-4">
            <div className="row g-5">
              <div className="col-12 feature wow rotateInDownLeft" data-wow-delay="0.2s">
                <div className="feature-item bg-primary rounded d-flex align-items-center justify-content-center mb-3">
                  <i className="fa fa-cubes text-white"></i>
                </div>
                <h4>Best In Industry</h4>
                <p className="mb-0">The way with innovative and sustainable solutions for a brighter and more energy-efficient world.</p>
              </div>
              <div className="col-12 feature wow fadeInLeft " data-wow-delay="0.6s">
                <div className="feature-item bg-primary rounded d-flex align-items-center justify-content-center mb-3">
                  <i className="fa fa-award text-white"></i>
                </div>
                <h4>Award Winning</h4>
                <p className="mb-0">With our award-winning achievements and recognition, we continue to excel in delivering exceptional products and services.</p>
              </div>
            </div>
          </div>
          <div className="col-lg-4  wow fadeInDown feature-box" data-wow-delay=".9s">
            <div className="position-relative h-100">
              <img className="position-absolute w-100 h-100 rounded wow zoomIn border-rounder" data-wow-delay="0.1s" src="img/header_1.jpg" alt="img" />
            </div>
          </div>
          <div className="col-lg-4">
            <div className="row g-5">
              <div className="col-12 feature wow fadeInRightBig " data-wow-delay="0.4s">
                <div className="feature-item bg-primary rounded d-flex align-items-center justify-content-center mb-3">
                  <i className="fa fa-users-cog text-white"></i>
                </div>
                <h4>Professional Staff</h4>
                <p className="mb-0">Our professional staff, armed with expertise and dedication, ensures top-notch service and excellence in all we do.</p>
              </div>
              <div className="col-12 feature wow fadeInRightBig " data-wow-delay="0.8s">
                <div className="feature-item bg-primary rounded d-flex align-items-center justify-content-center mb-3">
                  <i className="fa fa-phone-alt text-white"></i>
                </div>
                <h4>24/7 Support</h4>
                <p className="mb-0">Our dedicated team ensures 24/7 support to assist you anytime, anywhere.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
