import React from "react";
import OwlCarousel from "react-owl-carousel";
import "./Suppliers.scss";
const Suppliers = () => {
  const options = {
    loop: true,
    margin: 2,
    center: true,
    autoplaySpeed: 6000,
    autoplayTimeout: 2500,
    nav: false,
    dots: false,
    autoplay: true,
    responsive: {
      0: {
        items: 2,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 7,
      },
    },
  };
  return (
    <div className="container-fluid">
      <div className="container suppliers py-2 px-2">
        <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
          <h5 className="fw-bold text-primary text-uppercase">Our Suppliers</h5>
        </div>
        <OwlCarousel {...options} className="owl-theme">
          <div className="supplier-item">
            <img src="./img/suppliers/luminous.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/deye.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/du.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/exide.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/felicity.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/growat.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/jinko.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/kinfon.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/longi.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/must.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/orbite.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/rocket.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/seabang.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/souer.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/super_power.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/threek.jpg" className="img-fluid" alt="" />
          </div>
          <div className="supplier-item">
            <img src="./img/suppliers/tuff.jpg" className="img-fluid" alt="" />
          </div>
        </OwlCarousel>
      </div>
    </div>
  );
};

export default Suppliers;
