/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./TeamMember.scss";
import OwlCarousel from "react-owl-carousel";
import { Members } from "../../../Data/Data";
const TeamMember = () => {
  const options = {
    loop: true,
    margin: 2,
    nav: false,
    dots: true,
    center: true,
    autoplaySpeed: 5000,
    autoplayTimeout: 8000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 5,
      },
    },
  };
  return (
    <div className="team-wrapper">
      <div className="container-fluid team-container  wow fadeInUp" data-wow-delay="0.1s" id="team">
        <div className="container py-2">
          <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
            <h5 className="fw-bold text-primary text-uppercase">Team Members</h5>
          </div>
          <div className="row g-5">
            <OwlCarousel {...options} className="owl-theme ">
              {Members.map((member, index) => (
                <div key={index} className="col-lg-12 item-member wow fadeInLeft border-rounder" data-wow-delay="0.3s">
                  <div className="team-item bg-light rounded overflow-hidden">
                    <div className="team-img position-relative overflow-hidden">
                      <img className="img-fluid img-member w-100" src={member.imageSrc} alt="" />
                      <div className="team-social">
                        {member.SocialMedia.map((item, indexS) => (
                          <div key={indexS}>
                            {item.name === "Email" ? (
                              <a className="btn btn-lg btn-primary btn-lg-square rounded" href={`mailto:${item.link}`} target="_blank" rel="noreferrer">
                                <i className={`${item.icon}  fw-normal`}></i>
                              </a>
                            ) : (
                              <a className="btn btn-lg btn-primary btn-lg-square rounded" href={item.link} target="_blank" rel="noreferrer">
                                <i className={`${item.icon}  fw-normal`}></i>
                              </a>
                            )}
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="text-center member-footer">
                      <h4 className="text-primary">{member.name}</h4>
                      <p className="text-uppercase m-0">{member.position}</p>
                    </div>
                  </div>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TeamMember;
