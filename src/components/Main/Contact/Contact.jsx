import React from "react";
import { useState } from "react";
import { addMessage } from "../../../api";
import { useEffect } from "react";

const Contact = () => {
  const initialContactModel = {
    username: "",
    email: "",
    subject: "",
    message: "",
    isGlobal: true,
  };
  const [contactModel, setContactModel] = useState(initialContactModel);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setContactModel((prev) => ({ ...prev, [name]: value }));
  };

  const sendMessage = () => {
    addMessage(contactModel)
      .then(async (response) => {
        setContactModel(initialContactModel);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    // Reset form values when contactModel changes
    setContactModel(initialContactModel);
  }, [contactModel]);
  return (
    <div>
      <section id="contact" className="contact">
        {/* <!--  Section Title --> */}
        <div className="container section-title" data-aos="fade-up">
          <h2>Contact</h2>
          <p>Necessitatibus eius consequatur ex aliquid fuga eum quidem sint consectetur velit</p>
        </div>
        {/* <!-- End Section Title --> */}
        <div className="container" data-aos="fade-up" data-aos-delay="100">
          <div className="row gy-4">
            <div className="col-lg-6">
              <div className="row gy-4">
                <div className="col-md-6">
                  <div className="info-item" data-aos="fade" data-aos-delay="200">
                    <i className="bi bi-geo-alt"></i>
                    <h3>Address</h3>
                    <p>A108 Adam Street</p>
                    <p>New York, NY 535022</p>
                  </div>
                </div>
                {/* <!-- End Info Item --> */}
                <div className="col-md-6">
                  <div className="info-item" data-aos="fade" data-aos-delay="300">
                    <i className="bi bi-telephone"></i>
                    <h3>Call Us</h3>
                    <p>+1 5589 55488 55</p>
                    <p>+1 6678 254445 41</p>
                  </div>
                </div>
                {/* <!-- End Info Item --> */}
                <div className="col-md-6">
                  <div className="info-item" data-aos="fade" data-aos-delay="400">
                    <i className="bi bi-envelope"></i>
                    <h3>Email Us</h3>
                    <p>info@example.com</p>
                    <p>contact@example.com</p>
                  </div>
                </div>
                {/* <!-- End Info Item --> */}
                <div className="col-md-6">
                  <div className="info-item" data-aos="fade" data-aos-delay="500">
                    <i className="bi bi-clock"></i>
                    <h3>Open Hours</h3>
                    <p>Monday - Friday</p>
                    <p>9:00AM - 05:00PM</p>
                  </div>
                </div>
                {/* <!-- End Info Item --> */}
              </div>
            </div>
            <div className="col-lg-6">
              <form className="php-email-form" data-aos="fade-up" data-aos-delay="200">
                <div className="row gy-4">
                  <div className="col-md-6">
                    <input type="text" name="username" value={contactModel.username} onChange={handleChange} className="form-control" placeholder="Your Name" required />
                  </div>
                  <div className="col-md-6 ">
                    <input type="email" className="form-control" value={contactModel.email} name="email" onChange={handleChange} placeholder="Your Email" required />
                  </div>
                  <div className="col-md-12">
                    <input type="text" className="form-control" value={contactModel.subject} name="subject" onChange={handleChange} placeholder="Subject" required />
                  </div>
                  <div className="col-md-12">
                    <textarea className="form-control" name="message" value={contactModel.message} rows="6" onChange={handleChange} placeholder="Message" required></textarea>
                  </div>
                  <div className="col-md-12 text-center">
                    <button type="button" onClick={() => sendMessage()}>
                      Send Message
                    </button>
                  </div>
                </div>
              </form>
            </div>
            {/* <!-- End Contact Form --> */}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Contact;
