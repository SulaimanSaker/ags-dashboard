import React, { useEffect, useState } from "react";
import "./AllBlogs.scss";
import Heading from "../Heading/Heading";
import { getBlogs } from "../../api";
import { getWordsFromSentence } from "../../helper/getWordsFromSentence";
import { useNavigate } from "react-router-dom";
const AllBlogs = () => {
  const navigate = useNavigate();
  window.scrollTo({ top: 0, behavior: "smooth" });
  const [blogs, setBlogs] = useState([]);
  useEffect(() => {
    getBlogs()
      .then((response) => {
        setBlogs(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const getDate = (originalDateString) => {
    const originalDate = new Date(originalDateString);
    // Extract the year, month, and day from the original date
    const year = originalDate.getUTCFullYear(); // 2023
    const month = originalDate.getUTCMonth() + 1; // Month is zero-based, so add 1 to get the correct month
    const day = originalDate.getUTCDate(); // 27

    // Format the date string as "01 Jan, 2045"
    const formattedDate = `${day.toString().padStart(2, "0")} ${getMonthAbbreviation(month)}, ${year}`;

    return formattedDate;
  };
  // Helper function to get the abbreviated month name
  function getMonthAbbreviation(month) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return months[month - 1];
  }
  return (
    <div>
      <Heading headingInfo={{ heading: "Blogs", title: "Home", subTitle: "Blogs" }} />
      <div className="container-fluid py-5 pt-0 wow fadeInUp" data-wow-delay="0.1s" id="blogs_id">
        <div className="blogs-wrapper">
          <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
            <h5 className="fw-bold text-primary text-uppercase">Latest Blog</h5>
          </div>
          <div className="row g-5">
            <div className="col-lg-12">
              <div className="row g-5">
                {blogs.map((blog, index) => (
                  <div key={index} className="col-md-3 wow slideInUp" data-wow-delay="0.1s" onClick={() => navigate("/blog-detail", { state: blog })}>
                    <div className="blog-item bg-light rounded overflow-hidden">
                      <div className="blog-img position-relative overflow-hidden">
                        <img className="img-fluid" src={blog.file} alt="" />
                      </div>
                      <div className="p-4">
                        <div className="d-flex mb-3">
                          <small className="me-3">
                            <i className="far fa-user text-primary me-2"></i>Mohammad Chhada
                          </small>
                          <small>
                            <i className="far fa-calendar-alt text-primary me-2"></i>
                            {getDate(blog.date)}
                          </small>
                        </div>
                        <h4 className="mb-3">{getWordsFromSentence(blog.title, 4)}</h4>
                        <p>{getWordsFromSentence(blog.description, 10)}....</p>
                        <a className="text-uppercase" onClick={() => navigate("/blog-detail", { state: blog })}>
                          Read More <i className="bi bi-arrow-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllBlogs;
