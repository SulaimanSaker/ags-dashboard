import React, { useState } from "react";
import "./Footer.scss";
import { Link } from "react-router-dom";
import { addMessage } from "../../api";
const Footer = () => {
  const year = new Date().getFullYear();
  const [activeLabel, setActiveLabel] = useState("");
  const [emailSubscribe, setEmailSubscribe] = useState("");
  const jumpToBlock = (id) => {
    setActiveLabel(id);
    setTimeout(() => {
      document.getElementById(id).scrollIntoView({ behavior: "smooth" });
    }, 500);
  };
  const sendMessage = () => {
    let model = {
      username: "Test",
      email: emailSubscribe,
      subject: "For Subscription",
      message: "this is from footer for subscribe in our newsletter",
      isGlobal: false,
    };
    addMessage(model)
      .then((response) => {
        setEmailSubscribe("");
      })
      .catch((err) => {});
  };
  return (
    <>
      <div className="container-fluid bg-dark text-light  wow fadeInUp" data-wow-delay="0.1s">
        <div className="container">
          <div className="row gx-5">
            <div className="col-lg-4 col-md-6 footer-about">
              <div className="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-primary p-4">
                <a className="navbar-brand">
                  <h2 className="m-0 text-white">AGS International</h2>
                </a>
                <p className="mt-3 mb-4">
                  stay updated with all the latest news, articles, and updates from our website. Simply enter your email address below to subscribe to our newsletter.
                </p>
                <form>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control border-white p-3"
                      name="emailSubscribe"
                      onChange={(e) => setEmailSubscribe(e.target.value)}
                      placeholder="Your Email"
                    />
                    <button type="button" className="btn btn-dark" onClick={() => sendMessage()}>
                      Subscribe
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="col-lg-8 col-md-6">
              <div className="row gx-5">
                <div className="col-lg-4 col-md-12 pt-5 mb-5">
                  <div className="section-title section-title-sm position-relative pb-3 mb-4">
                    <h3 className="text-light mb-0">Get In Touch</h3>
                  </div>
                  <div className="d-flex mb-2">
                    <i className="bi bi-geo-alt text-primary me-2"></i>
                    <p className="mb-0">
                      {" "}
                      <a className="bar-item me-3 text-light" href="https://goo.gl/maps/D61uNbU9abQZ6CPZ6" target="_blank" rel="noreferrer">
                        11 Street 25 - Naif - Dubai
                      </a>
                    </p>
                  </div>
                  <div className="d-flex mb-2">
                    <i className="bi bi-envelope-open text-primary me-2"></i>
                    <p className="mb-0">
                      {" "}
                      <a className=" bar-item text-light" href="mailTo:info@ags.ac" target="_blank" rel="noreferrer">
                        info@ags.ac
                      </a>
                    </p>
                  </div>
                  <div className="d-flex mb-2">
                    <i className="bi bi-telephone text-primary me-2"></i>
                    <p className="mb-0">
                      <a className="bar-item me-3 text-light" href="tel:+971542899793" target="_blank" rel="noreferrer">
                        +971 54 289 9793
                      </a>
                    </p>
                  </div>
                  <div className="d-flex mt-4">
                    <a className="btn btn-primary btn-square me-2" target="_blank" rel="noreferrer" href="https://www.facebook.com/profile.php?id=100090021907769">
                      <i className="fab fa-facebook-f fw-normal"></i>
                    </a>
                    <a className="btn btn-primary btn-square me-2" target="_blank" rel="noreferrer" href="https://ae.linkedin.com/company/ags-international-general-trading">
                      <i className="fab fa-linkedin-in fw-normal"></i>
                    </a>
                    <a className="btn btn-primary btn-square me-2" target=" _blank" rel="noreferrer" href="https://www.instagram.com">
                      <i className="fab fa-instagram fw-normal"></i>
                    </a>
                    <a
                      className="btn btn-primary btn-square me-2"
                      target="_blank"
                      href="https://api.whatsapp.com/send/?phone=971542899793&text=Hello+AGS+i+need+your+help.&type=phone_number&app_absent=0"
                      rel="noreferrer"
                    >
                      <i className="fab fa-whatsapp fw-normal"></i>
                    </a>
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                  <div className="section-title section-title-sm position-relative pb-3 mb-4">
                    <h3 className="text-light mb-0">Quick Links</h3>
                  </div>
                  <div className="link-animated d-flex flex-column justify-content-start">
                    <Link className="text-light mb-2 c_pointer" href="#">
                      <i className="bi bi-arrow-right text-primary me-2"></i>Home
                    </Link>
                    <Link className="text-light mb-2 c_pointer" to="/" onClick={() => jumpToBlock("services_id")}>
                      <i className="bi bi-arrow-right text-primary me-2"></i>Our Services
                    </Link>
                    <Link className="text-light mb-2 c_pointer" to="/" onClick={() => jumpToBlock("team")}>
                      <i className="bi bi-arrow-right text-primary me-2"></i>Meet The Team
                    </Link>
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                  <div className="section-title section-title-sm position-relative pb-3 mb-4">
                    <h3 className="text-light mb-0">Popular Links</h3>
                  </div>
                  <div className="link-animated d-flex flex-column justify-content-start">
                    <Link className="text-light mb-2 c_pointer" to="products">
                      <i className="bi bi-arrow-right text-primary me-2"></i>Products
                    </Link>
                    <Link className="text-light mb-2 c_pointer" to="/blogs">
                      <i className="bi bi-arrow-right text-primary me-2"></i>Blogs
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid text-white copy-right ">
        <div className="container text-center">
          <div className="row justify-content-end">
            <div className="col-lg-8 col-md-6">
              <div className="d-flex align-items-center justify-content-center copy-right-content">
                <small className="mb-0">
                  &copy;{" "}
                  <a className="text-white border-bottom" href="#">
                    AGS International
                  </a>
                  <span>
                    {" "}
                    <span>{year}</span> – All Rights Reserved
                  </span>
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
