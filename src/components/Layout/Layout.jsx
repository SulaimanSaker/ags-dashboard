import React from "react";
import Topbar from "../Topbar/Topbar";
import Header from "../Header/Header";
import Suppliers from "../Main/Suppliers/Suppliers";
import Footer from "../Footer/Footer";
import { Outlet } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import Contact from "../Contact/Contact";
import NewsBar from "../NewsBar/NewsBar";

const Layout = () => {
  return (
    <div>
      <Navbar />
      <Outlet />
      <Footer />
    </div>
  );
};

export default Layout;
