import React from "react";
import "./Blogs.scss";
import { Link } from "react-router-dom";
const Blogs = () => {
  return (
    <div>
      <div className="container-fluid py-5 wow fadeInUp blogs" id="blogs" data-wow-delay="0.1s">
        <div className="container py-1">
          <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
            <h5 className="fw-bold text-primary text-uppercase">Latest Blog</h5>
            <h1 className="mb-0">Read The Latest Articles from Our Blog Post</h1>
          </div>
          <div className="row g-5">
            <div className="col-lg-4 wow rotateInUpLeft" data-wow-delay="0.3s">
              <div className="blog-item bg-light rounded overflow-hidden border-rounder">
                <div className="blog-img position-relative overflow-hidden">
                  <img className="img-fluid" src="https://source.unsplash.com/600x400/?solar" alt="" />
                  <a className="position-absolute top-0 start-0 bg-primary text-white rounded-end-custom rounded-end mt-5 py-2 px-4">Solar Panels </a>
                </div>
                <div className="p-4">
                  <div className="d-flex mb-3">
                    <small className="me-3">
                      <i className="far fa-user text-primary me-2"></i>John Doe
                    </small>
                    <small>
                      <i className="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023
                    </small>
                  </div>
                  <h4 className="mb-3">The Benefits of Solar Energy for Your Home</h4>
                  <p>In this blog post, you can discuss the numerous advantages of switching to solar energy for residential purposes</p>
                  <Link to="blog-detail" className="text-uppercase" href="">
                    Read More <i className="bi bi-arrow-right"></i>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4 wow slideInUp " data-wow-delay="0.6s">
              <div className="blog-item bg-light rounded overflow-hidden border-rounder">
                <div className="blog-img position-relative overflow-hidden">
                  <img className="img-fluid" src="https://source.unsplash.com/600x400/?solar" alt="" />
                  <a className="position-absolute top-0 start-0 bg-primary text-white rounded-end-custom rounded-end mt-5 py-2 px-4">Solar Panels </a>
                </div>
                <div className="p-4">
                  <div className="d-flex mb-3">
                    <small className="me-3">
                      <i className="far fa-user text-primary me-2"></i>John Doe
                    </small>
                    <small>
                      <i className="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2045
                    </small>
                  </div>
                  <h4 className="mb-3">Choosing the Right Solar Panel System</h4>
                  <p>This blog post can provide a detailed guide on how to select the best solar panel system for specific energy needs</p>
                  <Link className="text-uppercase" to="blog-detail">
                    Read More <i className="bi bi-arrow-right"></i>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4 wow rotateInDownRight " data-wow-delay="0.9s">
              <div className="blog-item bg-light rounded overflow-hidden border-rounder">
                <div className="blog-img position-relative overflow-hidden">
                  <img className="img-fluid" src="https://source.unsplash.com/600x400/?solar" alt="" />
                  <a className="position-absolute top-0 start-0 bg-primary text-white rounded-end-custom mt-5 py-2 px-4">Solar Panels </a>
                </div>
                <div className="p-4">
                  <div className="d-flex mb-3">
                    <small className="me-3">
                      <i className="far fa-user text-primary me-2"></i>John Doe
                    </small>
                    <small>
                      <i className="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2045
                    </small>
                  </div>
                  <h4 className="mb-3">Solar Power Maintenance Tips: Keeping Your Investment</h4>
                  <p>Offer valuable insights into the importance of solar panel maintenance and how to ensure optimal performance over the years</p>
                  <Link className="text-uppercase" to="blog-detail">
                    Read More <i className="bi bi-arrow-right"></i>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blogs;
