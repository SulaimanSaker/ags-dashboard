/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./NotFound.scss";
import { useNavigate } from "react-router-dom";
const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className="not-found-page">
      <h1>404</h1>
      <p>Oops! Something is wrong.</p>
      <a class="button" onClick={() => navigate("/")}>
        <i class="icon-home"></i> Go back in initial page, is better.
      </a>
    </div>
  );
};

export default NotFound;
