import React from "react";
import "./Testimonial.scss";
import OwlCarousel from "react-owl-carousel";
const Testimonial = () => {
  const options = {
    loop: true,
    margin: 8,
    center: true,
    autoplaySpeed: 3000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 3,
      },
    },
  };
  return (
    <>
      <div className="container py-5" id="testimonials_Id">
        <div className="section-title text-center position-relative pb-3 mb-4 mx-auto">
          <h5 className="fw-bold text-primary text-uppercase">Testimonial</h5>
          <h1 className="mb-0">What Our Clients Say About Solar Power Services</h1>
        </div>
        <div className="container testimonials">
          <OwlCarousel {...options} classNameName="owl-theme">
            <div className="testimonial-item bg-light my-4">
              <div className="testimonial-item d-flex align-items-center border-bottom text-heading">
                <img className="img-fluid testimonial-img rounded" src="img/testimonial-1.jpg" alt="" />
                <div className="ps-4">
                  <h4 className="text-primary mb-1">Client Name</h4>
                  <small className="text-uppercase">Profession</small>
                </div>
              </div>
              <div className="text-contain"> Top-notch solar products from AGS International. Impressed by quality and savings</div>
            </div>
            <div className="testimonial-item bg-light  my-4 ">
              <div className="d-flex align-items-center border-bottom text-heading ">
                <img className="img-fluid testimonial-img rounded" src="img/testimonial-2.jpg" alt="" />
                <div className="ps-4">
                  <h4 className="text-primary mb-1">Client Name</h4>
                  <small className="text-uppercase">Profession</small>
                </div>
              </div>
              <div className="text-contain">Solar energy made simple with AGS International. Their products are efficient, reliable, and worth every penny.</div>
            </div>
            <div className="testimonial-item bg-light my-4">
              <div className="d-flex align-items-center border-bottom  text-heading ">
                <img className="img-fluid testimonial-img rounded" src="img/testimonial-3.jpg" alt="" />
                <div className="ps-4">
                  <h4 className="text-primary mb-1">Client Name</h4>
                  <small className="text-uppercase">Profession</small>
                </div>
              </div>
              <div className="text-contain">Thanks to AGS International, I now enjoy solar-powered living. Their products are excellent, and my savings are remarkable</div>
            </div>
            <div className="testimonial-item bg-light my-4">
              <div className="d-flex align-items-center border-bottom text-heading">
                <img className="img-fluid testimonial-img rounded" src="img/testimonial-4.jpg" alt="" />
                <div className="ps-4">
                  <h4 className="text-primary mb-1">Client Name</h4>
                  <small className="text-uppercase">Profession</small>
                </div>
              </div>
              <div className="text-contain">Thanks to AGS International, I now enjoy solar-powered living. Their products are excellent, and my savings are remarkable</div>
            </div>
          </OwlCarousel>
        </div>
      </div>
    </>
  );
};

export default Testimonial;
