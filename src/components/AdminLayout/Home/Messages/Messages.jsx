import React from "react";
import "./Messages.scss";
import { Accordion, AccordionTab } from "primereact/accordion";
import BreadCrumbCustom from "../../BreadCrumbCustom/BreadCrumbCustom";
import { useState } from "react";
import { useEffect } from "react";
import { getMessages } from "../../../../api";
import { convertDateFormat } from "../../../../helper/FormateDate";
const Messages = () => {
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    getMessages()
      .then((response) => {
        setMessages(response.data);
      })
      .catch((err) => {});
  }, []);
  const items = [
    {
      label: "Messages",
      icon: "pi pi-fw pi-send",
      url: "/admin-ags-793/messages",
    },
  ];
  const home = { icon: "pi pi-home", url: "/admin-ags-793/messages" };
  return (
    <>
      <BreadCrumbCustom items={items} home={home} isHasButton={false} path="/admin-ags-793/new-blog" />
      <div className="container mt-5">
        <div className="card border-0">
          {messages?.map((item, index) => (
            <Accordion className="mt-2 mb-2">
              <AccordionTab header={item.subject}>
                <div className="d-flex justify-content-between mb-3">
                  <small>
                    <strong>User Name : </strong>
                    <span>{item.username}</span>
                  </small>
                  <small>
                    <strong>Date : </strong>
                    <span>{convertDateFormat(item.date)}</span>
                  </small>
                  <small>
                    <strong>Email : </strong> <span>{item.email}</span>
                  </small>
                </div>
                <p className="m-0">{item.message}</p>
              </AccordionTab>
            </Accordion>
          ))}
        </div>
      </div>
    </>
  );
};

export default Messages;
