import React from "react";
import "./Quotes.scss";
import { getQuotes } from "../../../../api/index.js";
import { useEffect } from "react";
import BreadCrumbCustom from "../../BreadCrumbCustom/BreadCrumbCustom";
import { useState } from "react";

const Quotes = () => {
  const [quotes, setQuotes] = useState([]);
  const [products, setProducts] = useState([]);
  const items = [
    {
      label: "Quotes",
      icon: "pi pi-fw pi-send",
      url: "/admin-ags-793/quotes",
    },
  ];
  const home = { icon: "pi pi-home", url: "/admin-ags-793/messages" };

  console.log("test", "quotes", quotes);

  useEffect(() => {
    getQuotes()
      .then((response) => {
        console.log(response.data);
        setQuotes(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <BreadCrumbCustom
        items={items}
        home={home}
        isHasButton={false}
        path="/admin-ags-793/new-blog"
      />
      <div className="container mt-5">
        <div className="card border-0">
          <table class="table table-bordered text-center">
            <thead>
              <tr className="table-light">
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Cash On Delivery</th>
                <th scope="col">Paid</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              {quotes?.map((item, index) => (
                <tr index={index}>
                  <th scope="row">{item.customer.name}</th>
                  <td>{item.customer.email}</td>
                  <td>{item.customer.phone}</td>
                  <td>{item?.isCashOnDelivery?.toString()}</td>
                  <td>{item?.isPaid?.toString()}</td>

                  <td>
                    <span
                      className=" btn-products-view"
                      data-bs-toggle="modal"
                      data-bs-target="#exampleModal"
                      onClick={() => setProducts(item.products)}
                    >
                      View Products
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title fs-5 mb-0" id="exampleModalLabel">
                Products
              </h3>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">
              <table class="table table-bordered text-center">
                <thead>
                  <tr className="table-light">
                    <th scope="col">Name</th>
                    <th scope="col">qty</th>
                  </tr>
                </thead>
                <tbody>
                  {products?.map((item, index) => (
                    <tr index={index}>
                      <th scope="row">
                        {item.product.category}
                        {item.product.code}
                        {item.product.capacity}
                      </th>
                      <td>{item.qty}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Quotes;
