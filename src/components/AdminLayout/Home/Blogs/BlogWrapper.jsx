import React from "react";
import "./BlogWrapper.scss";
import BreadCrumbCustom from "../../BreadCrumbCustom/BreadCrumbCustom";
import BlogItem from "./BlogItem/BlogItem";
import { useEffect } from "react";
import { getBlogs } from "../../../../api";
import { useState } from "react";
const items = [{ label: "Blogs" }];
const home = { icon: "pi pi-home", url: "/admin-ags-793/Blogs" };
const BlogWrapper = () => {
  const [blogs, setBlogs] = useState([]);
  useEffect(() => {
    getBlogs()
      .then((response) => {
        setBlogs(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      <BreadCrumbCustom
        items={items}
        home={home}
        isHasButton={true}
        path="/admin-ags-793/new-blog"
      />
      <div className="container-fluid  mt-5 mb-4">
        <div className="row mx-2 g-5">
          {blogs.map((blog, index) => (
            <BlogItem key={index} blog={blog} />
          ))}
        </div>
      </div>
    </>
  );
};

export default BlogWrapper;
