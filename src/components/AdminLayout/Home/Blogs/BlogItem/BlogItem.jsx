import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";
import { useState } from "react";
import { deleteBlog } from "../../../../../api";
import "./BlogItem.scss";
import { getWordsFromSentence } from "../../../../../helper/getWordsFromSentence";
const BlogItem = ({ blog }) => {
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);
  const [blogItem, setBlogItem] = useState({});
  const [position, setPosition] = useState("center");
  const show = (position) => {
    setPosition(position);
    setVisible(true);
  };
  const footerContent = (
    <div>
      <Button label="No" icon="pi pi-times" onClick={() => setVisible(false)} className="p-button-text" />
      <Button
        label="Yes"
        icon="pi pi-check"
        onClick={() => {
          setVisible(false);
          deleteBlogHandler();
        }}
        autoFocus
      />
    </div>
  );
  const deleteBlogHandler = () => {
    deleteBlog(blogItem._id)
      .then((response) => {})
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className="col-lg-3">
      <div className="blog-item bg-light rounded overflow-hidden border-rounder">
        <div className="blog-img position-relative overflow-hidden">
          <img className="img-fluid" src={blog.file} alt="" />
          <a className="position-absolute top-0 start-0 bg-primary text-white rounded-end-custom mt-5 py-2 px-4">{blog.category} </a>
        </div>
        <div className="p-4">
          <div className="d-flex mb-3">
            <small className="me-3">
              <i className="far fa-user text-primary me-2"></i>
              {blog.username}
            </small>
            <small>
              <i className="far fa-calendar-alt text-primary me-2"></i>
              {blog.date}
            </small>
          </div>
          <h4 className="mb-3">{getWordsFromSentence(blog.title, 4)}</h4>
          {/* <p>{getWordsFromSentence(blog.description, 14)}...</p> */}
          <div className="btn-edit-blog">
            <Button
              label="Top"
              icon="pi pi-trash"
              onClick={() => {
                show("top");
                setBlogItem(blog);
              }}
              className="p-button-danger btn-delete-blog mx-0"
            />
            <button onClick={() => navigate("/admin-ags-793/edit-blog", { state: blog })} className="btn-edit-blog">
              <i className="pi pi-file-edit"></i>
            </button>
          </div>
        </div>
      </div>
      <Dialog visible={visible} position={position} style={{ width: "30vw" }} onHide={() => setVisible(false)} footer={footerContent} draggable={false} resizable={false}>
        <p className="m-0">Are you sure from delete Blog?</p>
      </Dialog>
    </div>
  );
};

export default BlogItem;
