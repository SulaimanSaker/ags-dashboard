import React from "react";
import "./BlogWrapper.scss";
import BreadCrumbCustom from "../../BreadCrumbCustom/BreadCrumbCustom";
import BlogItem from "./BlogItem/BlogItem";
import { useEffect } from "react";
import { getServices } from "../../../../api";
import { useState } from "react";
const items = [{ label: "Services" }];
const home = { icon: "pi pi-home", url: "/admin-ags-793/services" };
const BlogWrapper = () => {
  const [blogs, setBlogs] = useState([]);
  useEffect(() => {
    getServices()
      .then((response) => {
        setBlogs(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      <BreadCrumbCustom
        items={items}
        home={home}
        isHasButton={true}
        path="/admin-ags-793/new-service"
      />
      <div className="container-fluid  mt-5 mb-4">
        <div className="row mx-2 g-5">
          {blogs.map((blog, index) => (
            <BlogItem key={index} blog={blog} />
          ))}
        </div>
      </div>
    </>
  );
};

export default BlogWrapper;
