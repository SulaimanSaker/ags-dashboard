import React, { useRef, useState } from "react";
import "./NewBlog.scss";
import BreadCrumbCustom from "../../../BreadCrumbCustom/BreadCrumbCustom";
import { Panel } from "primereact/panel";
import { Toast } from "primereact/toast";
import { FileUpload } from "primereact/fileupload";
import { ProgressBar } from "primereact/progressbar";
import { Button } from "primereact/button";
import { Tooltip } from "primereact/tooltip";
import { Tag } from "primereact/tag";
import { addService } from "../../../../../api";
import { useNavigate } from "react-router-dom";

const NewBlog = () => {
  const items = [{ label: "Blogs" }, { label: "New Blog" }];
  const home = { icon: "pi pi-home", url: "/admin-ags-793/blogs" };
  const toast = useRef(null);
  const navigate = useNavigate();
  const [totalSize, setTotalSize] = useState(0);
  const fileUploadRef = useRef(null);
  const [fileBlog, setFileBlog] = useState();
  const [blogModel, setBlogModel] = useState({
    title: "",
    file: "",
  });
  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = reject;
    });
  const onTemplateSelect = async (e) => {
    let _totalSize = totalSize;
    let files = e.files;
    Object.keys(files).forEach((key) => {
      _totalSize += files[key].size || 0;
    });
    setFileBlog(await toBase64(e.files[0]));
    setTotalSize(_totalSize);
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setBlogModel((prev) => ({ ...prev, [name]: value }));
  };
  const onTemplateUpload = (e) => {
    let _totalSize = 0;
    e.files.forEach((file) => {
      _totalSize += file.size || 0;
    });
    setTotalSize(_totalSize);
    toast.current.show({ severity: "info", summary: "Success", detail: "File Uploaded" });
  };

  const onTemplateRemove = (file, callback) => {
    setTotalSize(totalSize - file.size);
    callback();
    setFileBlog("");
  };

  const onTemplateClear = () => {
    setTotalSize(0);
    setFileBlog("");
  };

  const headerTemplate = (options) => {
    const { className, chooseButton } = options;
    const value = totalSize / 10000;
    const formatedValue =
      fileUploadRef && fileUploadRef.current ? fileUploadRef.current.formatSize(totalSize) : "0 B";

    return (
      <div
        className={className}
        style={{
          backgroundColor: "transparent",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div className="btn-action">{chooseButton}</div>
        <div className="header-uploader gap-3 ml-auto">
          <span>{formatedValue} / 1 MB</span>
          <ProgressBar
            value={value}
            showValue={false}
            style={{ width: "10rem", height: "12px" }}
          ></ProgressBar>
        </div>
      </div>
    );
  };

  const itemTemplate = (file, props) => {
    return (
      <div className="item-template">
        <div className="item-file" style={{ width: "40%" }}>
          <img alt={file.name} role="presentation" src={file.objectURL} width={100} />
          <span className="item-file-info">
            {file.name}
            <small>{new Date().toLocaleDateString()}</small>
          </span>
        </div>
        <Tag value={props.formatSize} severity="warning" className="px-3 py-2" />
        <Button
          type="button"
          icon="pi pi-times"
          className="p-button-outlined p-button-rounded p-button-danger ml-auto"
          onClick={() => onTemplateRemove(file, props.onRemove)}
        />
      </div>
    );
  };

  const emptyTemplate = () => {
    return (
      <div className="block-upload">
        <i
          className="pi pi-image mt-3 p-4"
          style={{
            fontSize: "3em",
            borderRadius: "50%",
            backgroundColor: "var(--surface-b)",
            color: "var(--surface-d)",
          }}
        ></i>
        <span style={{ fontSize: "1.2em", color: "var(--text-color-secondary)" }} className="my-3">
          Drag and Drop Image Blog Here
        </span>
      </div>
    );
  };

  const chooseOptions = {
    icon: "pi pi-fw pi-images",
    iconOnly: true,
    className: "custom-choose-btn p-button-rounded p-button-outlined",
  };

  const uploadOptions = {
    icon: "pi pi-fw pi-cloud-upload",
    iconOnly: true,
    className: "custom-upload-btn p-button-success p-button-rounded p-button-outlined",
  };

  const cancelOptions = {
    icon: "pi pi-fw pi-times",
    iconOnly: true,
    className: "custom-cancel-btn p-button-danger p-button-rounded p-button-outlined",
  };

  const addBlogHandle = () => {
    let model = {
      title: blogModel.title,
      date: new Date(),
      file: fileBlog,
    };

    addService(model)
      .then((response) => {
        navigate("/admin-ags-793/services");
      })
      .catch((error) => {});
  };

  const isValidate = () => {
    return blogModel.title !== "" && fileBlog !== "" ? true : false;
  };

  return (
    <div className="new-blog">
      <BreadCrumbCustom items={items} home={home} isHasButton={false} path="" />
      <div className="new-blog-content">
        <div className="container mt-5 mb-5">
          <Panel header="Service Information">
            <div className="row">
              <div className="col-lg-12">
                <div className="form-group">
                  <label className="mb-2" htmlFor="title ">
                    Title <span className="required">*</span>
                  </label>
                  <input
                    type="text"
                    id="title"
                    name="title"
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
              </div>

              <div className="col-lg-12 file-section">
                <div>
                  <Toast ref={toast}></Toast>
                  <Tooltip target=".custom-choose-btn" content="Choose" position="bottom" />
                  <FileUpload
                    ref={fileUploadRef}
                    name="demo[]"
                    url="/api/upload"
                    accept="image/*"
                    maxFileSize={1000000}
                    onUpload={onTemplateUpload}
                    onSelect={onTemplateSelect}
                    onError={onTemplateClear}
                    onClear={onTemplateClear}
                    headerTemplate={headerTemplate}
                    itemTemplate={itemTemplate}
                    emptyTemplate={emptyTemplate}
                    chooseOptions={chooseOptions}
                    uploadOptions={uploadOptions}
                    cancelOptions={cancelOptions}
                  />
                </div>
              </div>

              <div className="col-lg-12 btn-add-blog">
                <Button
                  label="Add Blog"
                  disabled={!isValidate()}
                  type="button"
                  icon="pi pi-check"
                  className="p-button-outlined"
                  onClick={() => addBlogHandle()}
                />
              </div>
            </div>
          </Panel>
        </div>
      </div>
    </div>
  );
};

export default NewBlog;
