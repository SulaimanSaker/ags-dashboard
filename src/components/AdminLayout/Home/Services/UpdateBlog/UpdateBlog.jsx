import React, { useRef, useState } from "react";
// import "./NewBlog.scss";
import BreadCrumbCustom from "../../../BreadCrumbCustom/BreadCrumbCustom";
import { Panel } from "primereact/panel";
import { Toast } from "primereact/toast";
import { FileUpload } from "primereact/fileupload";
import { ProgressBar } from "primereact/progressbar";
import { Button } from "primereact/button";
import { Tag } from "primereact/tag";
import { useLocation, useNavigate } from "react-router-dom";
import { updateService } from "../../../../../api";
import { Image } from "primereact/image";
import "./UpdateBlog.scss";
const UpdateBlog = () => {
  const { state } = useLocation();
  const navigate = useNavigate();
  const items = [{ label: "Blogs" }, { label: "Edit Blog" }];
  const home = { icon: "pi pi-home", url: "/admin-ags-793/blogs" };
  const toast = useRef(null);
  const [totalSize, setTotalSize] = useState(0);
  const [isDelete, setIsDelete] = useState(false);
  const fileUploadRef = useRef(null);
  const [fileBlog, setFileBlog] = useState(state.file);

  const [blogModel, setBlogModel] = useState({
    title: state.title,
    description: state.description,
    file: state.file,
    categories: state.categories,
  });
  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = reject;
    });
  const onTemplateSelect = async (e) => {
    let _totalSize = totalSize;
    let files = e.files;
    Object.keys(files).forEach((key) => {
      _totalSize += files[key].size || 0;
    });
    setFileBlog(await toBase64(e.files[0]));
    setTotalSize(_totalSize);
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setBlogModel((prev) => ({ ...prev, [name]: value }));
  };
  const onTemplateRemove = (file, callback) => {
    setTotalSize(totalSize - file.size);
    callback();
  };

  const headerTemplate = (options) => {
    const { className, chooseButton } = options;
    const value = totalSize / 10000;
    const formatedValue =
      fileUploadRef && fileUploadRef.current ? fileUploadRef.current.formatSize(totalSize) : "0 B";

    return (
      <div
        className={className}
        style={{
          backgroundColor: "transparent",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div className="btn-action">{chooseButton}</div>
        <div className="header-uploader gap-3 ml-auto">
          <span>{formatedValue} / 1 MB</span>
          <ProgressBar
            value={value}
            showValue={false}
            style={{ width: "10rem", height: "12px" }}
          ></ProgressBar>
        </div>
      </div>
    );
  };

  const itemTemplate = (file, props) => {
    return (
      <div className="item-template">
        <div className="item-file" style={{ width: "40%" }}>
          <img alt={file.name} role="presentation" src={file.objectURL} width={100} />
          <span className="item-file-info">
            {file.name}
            <small>{new Date().toLocaleDateString()}</small>
          </span>
        </div>
        <Tag value={props.formatSize} severity="warning" className="px-3 py-2" />
        <Button
          type="button"
          icon="pi pi-times"
          className="p-button-outlined p-button-rounded p-button-danger ml-auto"
          onClick={() => onTemplateRemove(file, props.onRemove)}
        />
      </div>
    );
  };

  const emptyTemplate = () => {
    return (
      <div className="block-upload">
        <i
          className="pi pi-image mt-3 p-4"
          style={{
            fontSize: "3em",
            borderRadius: "50%",
            backgroundColor: "var(--surface-b)",
            color: "var(--surface-d)",
          }}
        ></i>
        <span style={{ fontSize: "1.2em", color: "var(--text-color-secondary)" }} className="my-3">
          Drag and Drop Image Blog Here
        </span>
      </div>
    );
  };

  const chooseOptions = {
    icon: "pi pi-fw pi-images",
    iconOnly: true,
    className: "custom-choose-btn p-button-rounded p-button-outlined",
  };
  const updateBlogHandler = () => {
    let model = {
      title: blogModel.title,
      date: new Date(),
      file: fileBlog,
    };
    updateService(state._id, model)
      .then((response) => {
        navigate("/admin-ags-793/services");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const isValidate = () => {
    return blogModel.title !== "" && blogModel.file !== "" ? true : false;
  };
  return (
    <div className="new-blog">
      <BreadCrumbCustom items={items} home={home} isHasButton={false} path="" />
      <div className="new-blog-content">
        <div className="container mt-5 mb-5">
          <Panel header="Blog Information">
            <div className="row">
              <div className="col-lg-12">
                <div className="form-group">
                  <label className="mb-2" htmlFor="title ">
                    Title <span className="required">*</span>
                  </label>
                  <input
                    type="text"
                    id="title"
                    value={blogModel.title}
                    name="title"
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
              </div>

              {!isDelete && (
                <div className="img-view">
                  <div className="image-blog">
                    <Image src={blogModel.file} alt="Image" width="250" preview />
                    <i className="pi pi-trash" onClick={() => setIsDelete(true)}></i>
                  </div>
                </div>
              )}

              {isDelete && (
                <div className="col-lg-12 file-section">
                  <div>
                    <Toast ref={toast}></Toast>
                    <FileUpload
                      ref={fileUploadRef}
                      name="demo[]"
                      url="/api/upload"
                      accept="image/*"
                      maxFileSize={1000000}
                      onSelect={onTemplateSelect}
                      headerTemplate={headerTemplate}
                      itemTemplate={itemTemplate}
                      emptyTemplate={emptyTemplate}
                      chooseOptions={chooseOptions}
                    />
                  </div>
                </div>
              )}
              <div className="col-lg-12 btn-add-blog">
                <Button
                  label="Update Blog"
                  disabled={!isValidate()}
                  type="button"
                  icon="pi pi-check"
                  className="p-button-outlined"
                  onClick={() => updateBlogHandler()}
                />
              </div>
            </div>
          </Panel>
        </div>
      </div>
    </div>
  );
};

export default UpdateBlog;
