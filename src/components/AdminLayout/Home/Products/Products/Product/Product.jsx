import React from "react";
import "./Product.scss";
import { useNavigate } from "react-router-dom";
import { downloadDatasheet } from "../../../../../../actions/productActions";
import { updateProductVisibility } from "../../../../../../api";
const Product = (product) => {
  const navigate = useNavigate();
  const getPrice = (product) => {
    return (product?.productPrice * product?.usdRate + product?.productPriceMargin).toFixed(2);
  };
  const updateVisibility = () => {
    console.log(product?.product?.info?.systemVisible);
    updateProductVisibility(product.product._id, !product?.product.info?.systemVisible)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <>
      <div className="product" data-wow-delay="0.3s">
        <div className="card">
          <div
            className="edit-product"
            onClick={() => {
              navigate("/admin-ags-793/product-detail", { state: product });
            }}
          >
            <i className="uil uil-edit-alt "></i>
          </div>
          <div className="hide-product" onClick={() => updateVisibility()}>
            <span>{product?.product?.info?.systemVisible && <i className="uil uil-eye "></i>}</span>
            <span> {!product?.product?.info?.systemVisible && <i className="uil uil-eye-slash "></i>}</span>
          </div>
          <div className="imgBox">
            <img src={product?.product?.image[0]?.image} className="img-product" alt={`${product.product.category}-alt`} />
          </div>
          <div className="contentBox">
            <h5> {product.product.brand}</h5>
            <h6 style={{ fontSize: "16px", fontFamily: "monospace", fontWeight: "600", color: "rgb(197 19 19)" }}> {product.product.code}</h6>
            <h5 className="price">{product.product.capacity}</h5>
            <h5 className="price required">{getPrice(product.product.info)}</h5>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
