import React from "react";
import { countCharacters } from "../../../../../../../../../helper/countWords";

const ProductFeatures = ({ product, setProductModel }) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProductModel((prev) => ({ ...prev, [name]: value }));
  };
  return (
    <div className="product-description">
      <div className="d-flex mb-3">
        <div className="w-50 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Title 1
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureOneTitle}
              maxLength={50}
              name="productFeatureOneTitle"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureOneTitle)} / 50</span>
          </div>
        </div>
        <div className="w-75 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Description 1
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureOneDescription}
              maxLength={120}
              name="productFeatureOneDescription"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureOneDescription)} / 120 </span>
          </div>
        </div>
      </div>
      <div className="d-flex mb-3">
        <div className="w-50 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Title 2
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureTwoTitle}
              maxLength={50}
              name="productFeatureTwoTitle"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureTwoTitle)} / 50</span>
          </div>
        </div>
        <div className="w-75 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Description 2
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureTwoDescription}
              maxLength={120}
              name="productFeatureTwoDescription"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureTwoDescription)} / 120 </span>
          </div>
        </div>
      </div>
      <div className="d-flex mb-3">
        <div className="w-50 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Title 3
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureThreeTitle}
              maxLength={40}
              name="productFeatureThreeTitle"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureThreeTitle)} / 40</span>
          </div>
        </div>
        <div className="w-75 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Description 3
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureThreeDescription}
              maxLength={120}
              name="productFeatureThreeDescription"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureThreeDescription)} / 120 </span>
          </div>
        </div>
      </div>
      <div className="d-flex mb-3">
        <div className="w-50 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Title 4
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureFourTitle}
              maxLength={50}
              name="productFeatureFourTitle"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureFourTitle)} / 50</span>
          </div>
        </div>
        <div className="w-75 mx-2">
          <div class="input-group input-group-sm mb-1 mt-2">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Feature Description 4
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              value={product.productFeatureFourDescription}
              maxLength={120}
              name="productFeatureFourDescription"
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div className="footer-hint">
            <span>characters count</span>
            <span>{countCharacters(product.productFeatureFourDescription)} / 120 </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductFeatures;
