import * as React from "react";
import PropTypes from "prop-types";
import "./ProductFooter.scss";
import ProductDescription from "./ProductDescription/ProductDescription";
import ProductInformation from "./ProductInformation/ProductInformation";
import { Box, Tab, Tabs, Typography } from "@mui/material";
import ProductFeatures from "./ProductFeatures/ProductFeatures";
function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const ProductFooter = ({ product, setProductModel }) => {
  const [value, setValue] = React.useState(0);
  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className="container product-footer">
      <Box sx={{ width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs value={value} onChange={handleChangeTab} aria-label="basic tabs example">
            <Tab label="Description" {...a11yProps(0)} />
            <Tab label="Features" {...a11yProps(1)} />
            <Tab label="Additional Information" {...a11yProps(2)} />
          </Tabs>
        </Box>
        <CustomTabPanel value={value} index={0}>
          <ProductDescription product={product} setProductModel={setProductModel} />
        </CustomTabPanel>
        <CustomTabPanel value={value} index={1}>
          <ProductFeatures product={product} setProductModel={setProductModel} />
        </CustomTabPanel>
        <CustomTabPanel value={value} index={2}>
          <ProductInformation product={product} setProductModel={setProductModel} />
        </CustomTabPanel>
      </Box>
    </div>
  );
};
export default ProductFooter;
