import React from "react";
import { ShareSocial } from "react-share-social";
import "./SocialMedia.scss";
const SocialMedia = () => {
  const socialItems = ["facebook", "whatsapp", "linkedin"];
  const shareUrl = "https://ags.ac/";
  const onGetQuoteClick = (id, code, capacity) => {
    const phoneNumber = "971565527684";
    const message = "Hello AGS, I'm looking for more information about " + code + " with capacity " + capacity;
    const whatsappUrl = `https://wa.me/${phoneNumber}?text=${message}`;
    window.open(whatsappUrl);
  };
  return (
    <div>
      <ShareSocial socialTypes={socialItems} url={shareUrl} onSocialButtonClicked={(data) => console.log(data)} />
    </div>
  );
};

export default SocialMedia;
//
