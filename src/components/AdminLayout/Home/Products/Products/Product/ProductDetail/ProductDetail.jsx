import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./ProductDetail.scss";
import ProductFooter from "./ProductFooter/ProductFooter";
import BreadCrumbCustom from "../../../../../BreadCrumbCustom/BreadCrumbCustom";
import { updateInfoProduct } from "../../../../../../../api";
import { countCharacters } from "../../../../../../../helper/countWords";

const ProductDetailAdmin = () => {
  const { state } = useLocation();
  console.log("state", state);
  const navigate = useNavigate();
  const [product, setProduct] = useState(state.product);
  const [productModel, setProductModel] = useState({
    productTitle: product?.info?.productTitle,
    subTitle: product?.info?.subTitle ? product?.info?.subTitle : "",
    productDescription: product?.info?.productDescription,
    productIntroduction: product?.info?.productIntroduction,
    productSpecifications: product?.info?.productSpecifications,
    productFeature: product?.info?.productFeature,
    productSafetyFeatureOne: product?.info?.productSafetyFeatureOne ? product?.info?.productSafetyFeatureOne : "",
    productSafetyFeatureTwo: product?.info?.productSafetyFeatureTwo ? product?.info?.productSafetyFeatureTwo : "",
    productSafetyFeatureThree: product?.info?.productSafetyFeatureThree ? product?.info?.productSafetyFeatureThree : "",
    productFeatureOneTitle: product?.info?.productFeatureOneTitle ? product?.info?.productFeatureOneTitle : "",
    productFeatureTwoTitle: product?.info?.productFeatureTwoTitle ? product?.info?.productFeatureTwoTitle : "",
    productFeatureThreeTitle: product?.info?.productFeatureThreeTitle ? product?.info?.productFeatureThreeTitle : "",
    productFeatureFourTitle: product?.info?.productFeatureFourTitle ? product?.info?.productFeatureFourTitle : "",
    productFeatureOneDescription: product?.info?.productFeatureOneDescription ? product?.info?.productFeatureOneDescription : "",
    productFeatureTwoDescription: product?.info?.productFeatureTwoDescription ? product?.info?.productFeatureTwoDescription : "",
    productFeatureThreeDescription: product?.info?.productFeatureThreeDescription ? product?.info?.productFeatureThreeDescription : "",
    productFeatureFourDescription: product?.info?.productFeatureFourDescription ? product?.info?.productFeatureFourDescription : "",
    productWeight: product?.info?.productProperties[0].value,
    productWarranty: product?.info?.productProperties[1].value,
    productDimension: product?.info?.productProperties[2].value,
    productPallet: product?.info?.productProperties[3].value,
    productSize: product?.info?.productProperties[4].value,
    productWidth: product?.info?.productProperties[4].value,
    productHight: product?.info?.productProperties[4].value,
    productLength: product?.info?.productProperties[4].value,
    salePrice: product?.info?.productProperties[4].value,
    productPrice: product?.info?.productPrice ? product?.info?.productPrice : 0,
    productSalePrice: product?.info?.productSalePrice ? product?.info?.productSalePrice : 0,
    externalLink: product?.info?.externalLink ? product?.info?.externalLink : "",
    internalLink: product?.info?.internalLink ? product?.info?.internalLink : "",
    productPriceMargin: product?.info?.productPriceMargin ? product?.info?.productPriceMargin : 0,
    usdRate: product?.info?.usdRate ? product?.info?.usdRate : 3.674,
    productAvailability: product?.info?.productAvailability ? product?.info?.productAvailability : 0,
    amazonLink: product?.info?.amazonLink ? product?.info?.amazonLink : "",
    noonLink: product?.info?.noonLink ? product?.info?.noonLink : "",
    sharafdgLink: product?.info?.sharafdgLink ? product?.info?.sharafdgLink : "",
    mircrolessLink: product?.info?.mircrolessLink ? product?.info?.mircrolessLink : "",
  });
  const onGetQuoteClick = (id, code, capacity) => {
    const phoneNumber = "971565527684";
    const message = "Hello AGS, I'm looking for more information about " + product.code + " with capacity " + product.capacity;
    const whatsappUrl = `https://wa.me/${phoneNumber}?text=${message}`;
    window.open(whatsappUrl);
  };
  const items = [{ label: "Products" }, { label: `Product ${product.brand} ${product.code} ${product.capacity}` }];
  const home = { icon: "pi pi-home", url: "/admin-ags-793/products" };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProductModel((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };
  const saveProduct = () => {
    let model = {
      productTitle: productModel.productTitle,
      subTitle: productModel.subTitle,
      productDescription: productModel.productDescription,
      productIntroduction: productModel.productIntroduction,
      productSpecifications: productModel.productSpecifications,
      productFeature: productModel.productFeature,
      productSafetyFeatureOne: productModel.productSafetyFeatureOne,
      productSafetyFeatureTwo: productModel.productSafetyFeatureTwo,
      productSafetyFeatureThree: productModel.productSafetyFeatureThree,
      productFeatureOneTitle: productModel.productFeatureOneTitle,
      productFeatureTwoTitle: productModel.productFeatureTwoTitle,
      productFeatureThreeTitle: productModel.productFeatureThreeTitle,
      productFeatureFourTitle: productModel.productFeatureFourTitle,
      productFeatureOneDescription: productModel.productFeatureOneDescription,
      productFeatureTwoDescription: productModel.productFeatureTwoDescription,
      productFeatureThreeDescription: productModel.productFeatureThreeDescription,
      productFeatureFourDescription: productModel.productFeatureFourDescription,
      amazonLink: productModel.amazonLink,
      noonLink: productModel.noonLink,
      sharafdgLink: productModel.sharafdgLink,
      mircrolessLink: productModel.mircrolessLink,
      externalLink: productModel.externalLink,
      internalLink: productModel.internalLink,
      productPrice: Number(productModel.productPrice),
      productSalePrice: Number(productModel.productSalePrice),
      productPriceMargin: Number(productModel.productPriceMargin),
      usdRate: Number(productModel.usdRate),
      productAvailability: Number(productModel.productAvailability),
      productProperties: [
        { label: "Weight", value: Number(productModel.productWeight) },
        { label: "Warranty", value: Number(productModel.productWarranty) },
        { label: "Dimension", value: Number(productModel.productDimension) },
        { label: "Pallet", value: Number(productModel.productPallet) },
        { label: "Size", value: Number(productModel.productSize) },
        { label: "Width", value: Number(productModel.productWidth) },
        { label: "Height", value: Number(productModel.productHight) },
        { label: "Length", value: Number(productModel.productLength) },
      ],
    };
    console.log(model);
    updateInfoProduct(product._id, model)
      .then((response) => {
        navigate("/admin-ags-793/products");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div>
      <BreadCrumbCustom items={items} home={home} isHasButton={false} path="" />
      <div id="productDetail">
        <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
          <div className="container py-5">
            <div className="row g-5">
              <div className="col-lg-4 mt-0">
                <div className="img-box">
                  <img className="img-fluid w-100 rounded " src={product?.image[0]?.image} alt="" />
                </div>
                <div class="input-group input-group-sm mb-3 mt-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text sale-price" id="inputGroup-sizing-sm">
                      Sale Price
                    </span>
                  </div>
                  <input
                    type="text"
                    class="form-control"
                    aria-label="Small"
                    name="productSalePrice"
                    value={productModel.productSalePrice}
                    onChange={handleChange}
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </div>
                <div class="input-group input-group-sm mb-3 mt-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text sale-price" id="inputGroup-sizing-sm">
                      Sale Price
                    </span>
                  </div>
                  <input
                    type="text"
                    class="form-control"
                    aria-label="Small"
                    name="productSalePrice"
                    value={productModel.productSalePrice}
                    onChange={handleChange}
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">
                      External Link
                    </span>
                  </div>
                  <input
                    type="text"
                    class="form-control"
                    aria-label="Small"
                    name="externalLink"
                    value={productModel.externalLink}
                    onChange={handleChange}
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">
                      Internal Price
                    </span>
                  </div>
                  <input
                    type="text"
                    class="form-control"
                    aria-label="Small"
                    name="internalLink"
                    value={productModel.internalLink}
                    onChange={handleChange}
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </div>
              </div>
              <div className="col-lg-8 product-detail">
                <h1 className="mb-3">
                  <div className="w-100 flex justify-content-center">
                    <h5>
                      {productModel.productTitle} <strong className="required">{product.capacity} </strong>
                      --- <strong className="required">{product.grossWeight} Kg</strong>
                    </h5>
                  </div>
                </h1>
                <div className="row price-stock">
                  <div className="col-3">
                    <div class="input-group input-group-sm mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Price
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productPrice"
                        value={productModel.productPrice}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </div>
                  </div>
                  <div className="col-3">
                    <div class="input-group input-group-sm mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Margin
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productPriceMargin"
                        value={productModel.productPriceMargin}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </div>
                  </div>
                  <div className="col-3">
                    <div class="input-group input-group-sm mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          USD Rate
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="usdRate"
                        value={productModel.usdRate}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </div>
                  </div>
                  <div className="col-3 ">
                    <div class="input-group input-group-sm mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Stock
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productAvailability"
                        value={productModel.productAvailability}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <div class="input-group input-group-sm mb-1 mt-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          short description
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productDescription"
                        value={productModel.productDescription}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                        maxLength={500}
                      />
                    </div>
                    <div className="footer-hint">
                      <span>characters count</span>
                      <span>{countCharacters(productModel.productDescription)} / 500</span>
                    </div>
                    <div class="input-group input-group-sm mb-1 mt-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Feature Safety 1
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productSafetyFeatureOne"
                        value={productModel.productSafetyFeatureOne}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                        maxLength={40}
                      />
                    </div>
                    <div className="footer-hint mb-3">
                      <span>characters count</span>
                      <span>{countCharacters(productModel.productSafetyFeatureOne)} / 40</span>
                    </div>
                    <div class="input-group input-group-sm mb-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Feature Safety 2
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productSafetyFeatureTwo"
                        value={productModel.productSafetyFeatureTwo}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                        maxLength={40}
                      />
                    </div>
                    <div className="footer-hint mb-3">
                      <span>characters count</span>
                      <span>{countCharacters(productModel.productSafetyFeatureTwo)} / 40</span>
                    </div>
                    <div class="input-group input-group-sm mb-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">
                          Feature Safety 3
                        </span>
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        aria-label="Small"
                        name="productSafetyFeatureThree"
                        value={productModel.productSafetyFeatureThree}
                        onChange={handleChange}
                        aria-describedby="inputGroup-sizing-sm"
                        maxLength={40}
                      />
                    </div>
                    <div className="footer-hint mb-3">
                      <span>characters count</span>
                      <span>{countCharacters(productModel.productSafetyFeatureThree)} / 40</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>{" "}
        </div>
        <ProductFooter product={productModel} setProductModel={setProductModel} />
        <div className="btn-save-box">
          <button className="btn-save" onClick={() => saveProduct()}>
            Save
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductDetailAdmin;
