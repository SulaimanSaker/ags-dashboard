import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import "./ProductInformation.scss";
import React, { useState } from "react";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const ProductInformation = ({ product, setProductModel }) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProductModel((prev) => ({ ...prev, [name]: value }));
  };
  return (
    <div className="product-info">
      <table className="table table-sm  table-bordered">
        <thead>
          <tr className="table-active">
            <th scope="col">Property</th>
            <th scope="col">Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">
              <span className="propriety">Weight</span>
            </th>
            <td>
              <input type="text" className="form-control w-25 mx-auto" value={product.productWeight} name="productWeight" onChange={handleChange} />
            </td>
          </tr>
          <tr>
            <th scope="row">
              <span className="propriety">Warranty</span>
            </th>
            <td>
              <input type="text" className="form-control w-25 mx-auto" value={product.productWarranty} name="productWarranty" onChange={handleChange} />
            </td>
          </tr>
          <tr>
            <th scope="row">
              <span className="propriety">Width</span>
            </th>
            <td>
              <input type="text" className="form-control w-25 mx-auto" value={product.productWidth} name="productWidth" onChange={handleChange} />
            </td>
          </tr>
          <tr>
            <th scope="row">
              <span className="propriety">Height</span>
            </th>
            <td>
              <input type="text" className="form-control w-25 mx-auto" value={product.productHight} name="productHight" onChange={handleChange} />
            </td>
          </tr>
          <tr>
            <th scope="row">
              <span className="propriety">Length</span>
            </th>
            <td>
              <input type="text" className="form-control w-25 mx-auto" value={product.productLength} name="productLength" onChange={handleChange} />
            </td>
          </tr>
          <tr></tr>
        </tbody>
      </table>
    </div>
  );
};
export default ProductInformation;
