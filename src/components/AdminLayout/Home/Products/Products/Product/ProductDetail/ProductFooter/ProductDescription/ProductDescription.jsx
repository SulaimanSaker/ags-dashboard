import { InputTextarea } from "primereact/inputtextarea";
import React from "react";
import "./ProductDescription.scss";
const ProductDescription = ({ product, setProductModel }) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProductModel((prev) => ({ ...prev, [name]: value }));
  };
  return (
    <div className="product-description">
      <div className="mb-3">
        <>
          <h6>Social Media</h6>
          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text amazon-flied" id="inputGroup-sizing-sm">
                Amazon
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              aria-label="Small"
              name="amazonLink"
              value={product.amazonLink}
              onChange={handleChange}
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text noon-flied" id="inputGroup-sizing-sm">
                Noon
              </span>
            </div>
            <input type="text" class="form-control" name="noonLink" value={product.noonLink} aria-label="Small" onChange={handleChange} aria-describedby="inputGroup-sizing-sm" />
          </div>
          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                microless
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              name="mircrolessLink"
              value={product.mircrolessLink}
              onChange={handleChange}
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">
                Sharaf Dg
              </span>
            </div>
            <input
              type="text"
              class="form-control"
              name="sharafdgLink"
              value={product.sharafdgLink}
              onChange={handleChange}
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
            />
          </div>
        </>
      </div>
      <div className="mb-3">
        <h6>Introduction</h6>
        <div className="w-100 flex justify-content-center">
          <InputTextarea rows={5} cols={170} value={product.productIntroduction} name="productIntroduction" onChange={handleChange} />
        </div>
      </div>
      <div className="mb-3">
        <h6>Specifications</h6>
        <div className="w-100 flex justify-content-center">
          <InputTextarea rows={5} cols={170} value={product.productSpecifications} name="productSpecifications" onChange={handleChange} />
        </div>
      </div>
      <div className="mb-3">
        <h6>Feature</h6>
        <div className="w-100 flex justify-content-center">
          <InputTextarea rows={5} cols={170} value={product.productFeature} name="productFeature" onChange={handleChange} />
        </div>
      </div>
    </div>
  );
};

export default ProductDescription;
