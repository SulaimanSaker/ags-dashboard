import React, { useState } from "react";
import "./Products.scss";
import Product from "./Product/Product";
import SearchBox from "../SearchBox/SearchBox";
const Products = (products) => {
  return (
    <>
      {products && (
        <div className="products-grid" data-wow-delay="0.1s" id="team">
          {products.products?.map((product, index) => (
            <Product product={product} key={index} />
          ))}
        </div>
      )}
    </>
  );
};

export default Products;
