import React, { useEffect, useState } from "react";
import Products from "./Products/Products";
import { useDispatch, useSelector } from "react-redux";
import "./ProductsWrapper.scss";
import { getProducts } from "../../../../actions/productActions";
import BreadCrumbCustom from "../../BreadCrumbCustom/BreadCrumbCustom";
const ProductsWrapperAdmin = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
  const dispatch = useDispatch();
  const items = [{ label: "Products" }];
  const home = { icon: "pi pi-home", url: "/" };
  useEffect(() => {
    dispatch(getProducts(1, 800));
  }, [dispatch]);
  let allProducts = useSelector((state) => state.products.products);
  const [products, setProducts] = useState(allProducts?.products);
  const [searchQuery, setSearchQuery] = useState("");
  const [category, setCategory] = useState("all");

  useEffect(() => {
    // Filter products based on searchQuery
    let filteredProducts = allProducts?.products;
    if (searchQuery.trim() !== "") {
      console.log("searchQuery", filteredProducts);
      filteredProducts = filteredProducts.filter(
        (product) => product.brand.toLowerCase().includes(searchQuery.toLowerCase()) || product.capacity.toLowerCase().includes(searchQuery.toLowerCase())
      );
      console.log(filteredProducts);
    }
    // Filter products based on category
    if (category !== "all") {
      filteredProducts = filteredProducts.filter((product) => product.category.toLowerCase() === category.toLowerCase());
    }
    setProducts(filteredProducts);
  }, [searchQuery, allProducts?.products, category]);
  const handleSearchBoxChange = (e) => {
    setSearchQuery(e.target.value);
  };
  const handleFilterClick = (selectedCategory) => {
    setCategory(selectedCategory);
  };
  return (
    <>
      <BreadCrumbCustom items={items} home={home} isHasButton={false} path="" />
      <div className="container-fluid">
        <div className="search-container">
          <div className="col-12">
            <input type="text" className="form-control" placeholder="Search by brand or capacity" value={searchQuery} onChange={handleSearchBoxChange} />
          </div>
          <div className="col-12">
            <div className="group-buttons">
              <span
                className={`ags-btn-main-fill ${category === "all" ? "chosen_cat" : ""}`}
                onClick={() => {
                  handleFilterClick("all");
                }}
              >
                All
              </span>
              <span
                className={`ags-btn-main-fill ${category === "battery" ? "chosen_cat" : ""}`}
                onClick={() => {
                  handleFilterClick("battery");
                }}
              >
                Battery
              </span>
              <span
                className={`ags-btn-main-fill ${category === "solar" ? "chosen_cat" : ""}`}
                onClick={() => {
                  handleFilterClick("solar");
                }}
              >
                Solar
              </span>
              <span
                className={`ags-btn-main-fill ${category === "inverter" ? "chosen_cat" : ""}`}
                onClick={() => {
                  handleFilterClick("inverter");
                }}
              >
                Inverter
              </span>
            </div>
          </div>
        </div>
        {allProducts && <Products products={products ? products : []} />}
      </div>
    </>
  );
};

export default ProductsWrapperAdmin;
