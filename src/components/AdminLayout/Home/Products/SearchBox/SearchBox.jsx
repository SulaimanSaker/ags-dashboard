import React, { useState } from "react";

const SearchBox = ({ onCategoryChange: onCategoryChange }) => {
  const [category, setCategory] = useState("all");
  const handleFilterClick = (filter) => {
    onCategoryChange(filter);
    setCategory(filter);
  };
  return (
    <div className="header-website">
      <div className="row">
        <div className="col-lg-6 col-md-6">
          <div className="group-buttons">
            <span
              className={`ags-btn-main-fill ${category === "all" ? "chosen_cat" : ""}`}
              onClick={() => {
                handleFilterClick("all");
              }}
            >
              All
            </span>
            <span
              className={`ags-btn-main-fill ${category === "battery" ? "chosen_cat" : ""}`}
              onClick={() => {
                handleFilterClick("battery");
              }}
            >
              Battery
            </span>
            <span
              className={`ags-btn-main-fill ${category === "solar" ? "chosen_cat" : ""}`}
              onClick={() => {
                handleFilterClick("solar");
              }}
            >
              Solar
            </span>
            <span
              className={`ags-btn-main-fill ${category === "inverter" ? "chosen_cat" : ""}`}
              onClick={() => {
                handleFilterClick("inverter");
              }}
            >
              Inverter
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchBox;
