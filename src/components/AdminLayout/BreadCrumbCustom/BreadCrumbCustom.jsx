import React from "react";
import "./BreadCrumbCustom.scss";
import { BreadCrumb } from "primereact/breadcrumb";
import { Button } from "primereact/button";
import { useNavigate } from "react-router-dom";

const BreadCrumbCustom = ({ items, home, isHasButton, path }) => {
  const navigate = useNavigate();
  return (
    <div className="breadcrumb-wrapper">
      <BreadCrumb model={items} home={home} />
      {isHasButton && <Button className="pi pi-plus" onClick={() => navigate(path)}></Button>}
    </div>
  );
};
export default BreadCrumbCustom;
