import { Button } from "primereact/button";
import { Menubar } from "primereact/menubar";
import React from "react";
import "./Header.scss";
import { useNavigate } from "react-router-dom";
const Header = () => {
  const navigate = useNavigate();
  const items = [
    {
      label: "Products",
      icon: "pi pi-fw pi-slack",
      url: "/admin-ags-793/products",
    },
    {
      label: "Blogs",
      icon: "pi pi-fw pi-book",
      url: "/admin-ags-793/blogs",
    },
    {
      label: "Services",
      icon: "pi pi-fw pi-book",
      url: "/admin-ags-793/services",
    },
    {
      label: "Messages",
      icon: "pi pi-fw pi-send",
      url: "/admin-ags-793/messages",
    },
    {
      label: "Quotes",
      icon: "pi pi-fw pi-send",
      url: "/admin-ags-793/quotes",
    },
  ];
  const start = (
    <h4 className="logo">
      {" "}
      <span>AGS</span> Admin
    </h4>
  );
  const logout = () => {
    localStorage.clear();
    navigate("/");
  };
  return (
    <div className="layout">
      <div className="card">
        <Menubar model={items} start={start} />
        <div className="logout " onClick={() => logout()}>
          <Button icon="pi pi-fw pi-sign-out" value="Quit"></Button>
        </div>
      </div>
    </div>
  );
};

export default Header;
