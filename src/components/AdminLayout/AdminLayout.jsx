import React from "react";
import Header from "./Header/Header";
import SidBar from "./SidBar/SidBar";
import { PrimeReactProvider } from "primereact/api";
import Home from "./Home/Home.jsx";

const AdminLayout = () => {
  return (
    <div>
      <PrimeReactProvider>
        <Header />
        <SidBar />
        <Home />
      </PrimeReactProvider>
    </div>
  );
};

export default AdminLayout;
