import React from "react";
import "./Services.scss";
const Services = () => {
  return (
    <div className="services" id="services_id">
      <div className="container-fluid  wow rollIn" data-wow-delay="0.1s">
        <div className="container ">
          <div className="section-title text-center position-relative pb-3 mb-5 mx-auto">
            <h5 className="fw-bold text-primary text-uppercase">Our Services</h5>
          </div>
          <div className="row g-5">
            <div className="col-lg-4 col-md-6 wow rollIn " data-wow-delay="0.3s">
              <div className="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center border-rounder">
                <div className="service-icon">
                  <i className="fa fa-solar-panel text-white"></i>
                </div>
                <h4 className="mb-3">Project Planning</h4>
                <p className="m-0">Study and plan your project with our specialist team of engineers</p>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 wow fadeInDown " data-wow-delay="0.6s">
              <div className="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center border-rounder">
                <div className="service-icon">
                  <i className="fa fa-solar-panel text-white"></i>
                </div>
                <h4 className="mb-3">Best Batteries Inquiry</h4>
                <p className="m-0">Inquire about the best batteries for your residential and commercial</p>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 wow bounceInRight " data-wow-delay="0.9s">
              <div className=" service-item position-relative bg-primary rounded  d-flex flex-column align-items-center justify-content-center text-center  border-rounder">
                <h3 className="text-white mb-3">Call Us For Quote</h3>
                <p className="text-white mb-3">Call to ask any question</p>
                <h2 className="text-white mb-0">
                  {" "}
                  <a className="text-light fw-light" href="tel:+971542899793" rel="noreferrer">
                    +971 54 289 9793
                  </a>
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Services;
