import React from "react";
import "./CartItem.scss";
const getPrice = (product) => {
  return (product?.productPrice * product?.usdRate + product?.productPriceMargin).toFixed(2);
};
const getTotalItem = (qty, price) => {
  return (qty * price).toFixed(2);
};
const CartItem = ({ product }) => {
  return (
    <div className="cart-item">
      <div className="item">
        <div className="image item-box">
          <img src={product?.image[0]?.image} alt="product-img" />
        </div>
        <div className="buttons item-box">
        </div>
        <div className="description item-box">
          <span>
            {product?.brand} {product?.code} {product?.capacity}
          </span>
          <span>{product?.category}</span>
        </div>
        <div className="total-price item-box">{`${product?.qty}   x   ${getPrice(product?.info)} AED`}</div>
        <div className="total-price item-box total-item">{getTotalItem(product?.qty, getPrice(product?.info))} AED</div>
      </div>
    </div>
  );
};

export default CartItem;
