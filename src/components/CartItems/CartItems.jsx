import React, { useEffect, useState } from "react";
import "./CartItems.scss";
import { useDispatch, useSelector } from "react-redux";
import CartItem from "./CartItem/CartItem";
import { useNavigate } from "react-router-dom";
import { addOrder } from "../../api/index.js";
import { emptyCart } from "../../store/cartSlice.js";
import {  toast } from "react-toastify";
import { isMobile } from "react-device-detect";
import { InputSwitch } from "primereact/inputswitch";
const color = "#091E3E";
const Shipping_Cost = 0;
const CartItems = () => {
  const dispatch = useDispatch();
  const [checked, setChecked] = useState(false);
  const navigate = useNavigate();
  let products = useSelector((state) => state.cart.products);
  const [checkoutModel, setCheckoutModel] = useState({
    name: "",
    email: "",
    phone: "",
    address: "",
    location: "",
    products: [],
  });
  console.log("products", products);
  useEffect(() => {
    document.getElementById("nav-item").style.color = color;
    document.getElementById("contact-item").style.color = color;
    document.getElementById("services-item").style.color = color;
    document.getElementById("contact-item").style.color = color;
    document.getElementById("team-item").style.color = color;
    document.getElementById("blogs-id").style.color = color;
    document.getElementById("btn-login").style.borderColor = color;
    if (isMobile) {
      document.getElementById("cart").style.filter = "invert(0)";
      document.getElementById("cart").style.marginLeft = "auto";
      document.getElementById("cart").style.width = "10%";
    } else {
      document.getElementById("cart").style.filter = "invert(0)";
      document.getElementById("cart").style.marginLeft = "0";
      document.getElementById("cart").style.width = "40%";
    }
  }, []);
  function groupProductsById(products) {
    const productsById = {};
    // Iterate through the array of products
    for (const product of products) {
      // Extract _id from the current product
      const productId = product._id;
      // If the _id is already in productsById, increment the qty
      if (productsById[productId]) {
        productsById[productId].qty++;
      } else {
        // If not, create a new entry with qty 1
        productsById[productId] = { ...product, qty: 1 };
      }
    }

    // Convert the object values back to an array
    const groupedProducts = Object.values(productsById);

    // Return the array of products grouped by _id with qty field added
    return groupedProducts;
  }
  const groupedProducts = groupProductsById(products);
  const onChangeHandle = (e) => {
    const { name, value } = e.target;
    setCheckoutModel((prev) => ({ ...prev, [name]: value }));
  };
  const notify = (msg) =>
    toast.success(msg, {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  const addCheckout = () => {
    console.log(checkoutModel);
    let buffProducts = groupedProducts.map((product) => {
      return {
        product: product._id,
        qty: product.qty,
      };
    });
    let deepCopyProducts = JSON.parse(JSON.stringify(buffProducts));
    setCheckoutModel({ ...checkoutModel, products: deepCopyProducts });
    let model = {
      customer: {
        name: checkoutModel.name,
        email: checkoutModel.email,
        phone: checkoutModel.phone,
        address: checkoutModel.address,
        location: checkoutModel.location,
      },
      products: groupedProducts.map((product) => {
        return {
          product: product._id,
          qty: product.qty,
        };
      }),
    };
    addOrder(model)
      .then((response) => {
        console.log(response);
        notify("Thank you for your order! We've successfully received your order, and we'll be reaching out to you shortly.!");
        setTimeout(() => {
          dispatch(emptyCart());
          navigate("/products");
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const getPrice = (product) => {
    return (product?.productPrice * product?.usdRate + product?.productPriceMargin).toFixed(2);
  };
  function getTotalPrice(products) {
    let totalPriceWithoutVAT = 0;

    // Calculate total price without VAT
    for (const product of products) {
      totalPriceWithoutVAT += getPrice(product.info) * product.qty;
    }
    return totalPriceWithoutVAT.toFixed(2);
  }
  function calculateTotalPrice(products, vatRate) {
    let totalPriceWithoutVAT = 0;

    // Calculate total price without VAT
    for (const product of products) {
      totalPriceWithoutVAT += getPrice(product.info) * product.qty;
    }

    // Calculate VAT amount
    const vatAmount = totalPriceWithoutVAT * (vatRate / 100);

    // Calculate total price with VAT
    const totalPriceWithVAT = totalPriceWithoutVAT + vatAmount;

    // Return the total price with VAT
    return totalPriceWithVAT.toFixed(2);
  }
  return (
    <>
      <div className="container cart-wrapper" id="cart_id">
        <div className="row">
          <div className="col-lg-6">
            <div className="shopping-cart left-section">
              {/* Title */}
              <div className="title">
                <span className="back-btn" onClick={() => navigate("/products")}>
                  <i class="far fa-arrow-alt-circle-left"></i>
                </span>
                <span>Cart Shopping</span>{" "}
              </div>
              <>
                {groupedProducts?.length <= 0 && (
                  <div className="no-items" onClick={() => navigate("/products")}>
                    <img src="img/icons/empty-cart.png" className="empty-cart" alt="empty-cart" />
                    <p>No Items Added Yet!</p>
                  </div>
                )}
                {/* Product #2 */}
                {groupedProducts?.length > 0 && (
                  <div>
                    {groupedProducts.map((product, index) => (
                      <CartItem key={index} product={product} groupedProducts={groupedProducts} />
                    ))}
                  </div>
                )}
                <div className="summary">
                  <table class="table ">
                    <tbody>
                      <tr>
                        <th scope="row">
                          <div className="d-flex  justify-content-between">
                            <span className="item-name">Order Subtotal</span>
                            <span className="item-value"> {getTotalPrice(groupedProducts)} AED</span>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          <div className="d-flex  justify-content-between">
                            <span className="item-name"> Shipping and handling</span>
                            <span className="item-value"> {Shipping_Cost} AED</span>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="d-flex  justify-content-between">
                            <span className="item-name">VAT</span>
                            <span className="item-value">5 %</span>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td className="border-none">
                          <div className="d-flex  justify-content-between">
                            <span className="item-name">Total</span>
                            <span className="item-value"> {calculateTotalPrice(groupedProducts, 5)} AED</span>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="shopping-cart right-section">
              <div className="title">Checkout Information </div>
              <div className="summary-content">
                <form>
                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <label htmlFor="name_id">Name</label>
                        <input type="text" className="form-control" name="name" value={checkoutModel.name} onChange={onChangeHandle} />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group">
                        <label htmlFor="name_id">Email</label>
                        <input type="text" className="form-control" name="email" value={checkoutModel.email} onChange={onChangeHandle} />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group">
                        <label htmlFor="name_id">
                          Phone <span className="required">*</span>
                        </label>
                        <input type="text" className="form-control" name="phone" value={checkoutModel.phone} onChange={onChangeHandle} />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group">
                        <label htmlFor="name_id">Address</label>
                        <input type="text" className="form-control" name="address" value={checkoutModel.address} onChange={onChangeHandle} />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className=" my-4 switch-cart">
                        <span className="switch-cart-item d-flex mx-2">
                          <span className="required d-flex mx-1"> Cash</span> on delivery
                        </span>
                        <InputSwitch checked={checked} disabled={true} onChange={(e) => setChecked(e.value)} />
                        <span className=" switch-cart-item d-flex mx-2">Card payment (available soon)</span>
                      </div>
                    </div>
                    <div className="col-12">
                      <button type="button" className="ags-btn-main-fill" disabled={checkoutModel.phone === "" || products.length <= 0} onClick={() => addCheckout()}>
                        Checkout
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CartItems;
