import React from "react";
import "./Home.scss";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import Main from "../Main/Main";
const Home = () => {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
};

export default Home;
