import React from "react";
import "./Carousel.scss";
import { CarouselItems } from "../../Data/Data";
const Carousel = () => {
  return (
    <div className="header-carousel">
      <div id="header-carousel" className="carousel slide carousel-fade" data-bs-ride="carousel">
        <div className="carousel-inner">
          {CarouselItems.map((item, index) => (
            <div key={index} className={`carousel-item ${item.activeClass}`} data-bs-interval="4000">
              <img className="w-100" src={item.src} alt="img-1" />
              <div className={item.classNames}>
                <div className="carousel-content">
                  <h5 className={item.classNameTitle}>{item.title}</h5>
                  <h1 className={item.classNameDescription}>{item.description}</h1>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Carousel;
