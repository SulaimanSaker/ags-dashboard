import React from "react";

const FilterButtons = ({ handleFilterClick, category }) => {
  return (
    <div>
      <div className="group-buttons">
        <span
          className={`ags-btn-main-fill ${category === "all" ? "chosen_cat" : ""}`}
          onClick={() => {
            handleFilterClick("all");
          }}
        >
          {/* All */}
          <img src="./img/Icons/all.png" alt="all-icon" />
        </span>
        <span
          className={`ags-btn-main-fill ${category === "battery" ? "chosen_cat" : ""}`}
          onClick={() => {
            handleFilterClick("battery");
          }}
        >
          {/* Battery  */}
          <img src="./img/Icons/car-battery.png" alt="battery-img" />
        </span>
        <span
          className={`ags-btn-main-fill ${category === "solar" ? "chosen_cat" : ""}`}
          onClick={() => {
            handleFilterClick("solar");
          }}
        >
          {/* Solar */}
          <img src="./img/Icons/solar-cell.png" alt="solar-cell-img" />
        </span>
        <span
          className={`ags-btn-main-fill ${category === "inverter" ? "chosen_cat" : ""}`}
          onClick={() => {
            handleFilterClick("inverter");
          }}
        >
          {/* Inverter */}
          <img src="./img/Icons/inverter2.png" alt="inverter-icon" />
        </span>
      </div>
    </div>
  );
};

export default FilterButtons;
