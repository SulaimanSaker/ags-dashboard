import React, { useEffect, useState } from "react";
import Products from "./Products/Products";
import { useSelector } from "react-redux";
import "./ProductsWrapper.scss";
import Heading from "../Heading/Heading";
import Pagination from "@mui/material/Pagination";
import { filterProductsByBrandQuery } from "../../api";
import { Grid } from "react-loader-spinner";
import Navbar from "../Navbar/Navbar";
import FilterButtons from "./FilterButtons/FilterButtons";
import { isMobile } from "react-device-detect";
import { useRef } from "react";
const pageSize = 500;
const ProductsWrapper = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
  let [loading, setLoading] = useState(false);
  let [isGrid, setIsGrid] = useState(false);
  const [page, setPage] = React.useState(1);
  const [productsByBrands, setProductsByBrands] = useState([]);
  let allProducts = useSelector((state) => state.products);
  const [dataInfo, setDataInfo] = React.useState(allProducts.products);
  const [products, setProducts] = useState(allProducts?.products?.products);
  const [searchQuery, setSearchQuery] = useState("");
  const [category, setCategory] = useState("all");
  const listInnerRef = useRef();
  useEffect(() => {
    if (isMobile) {
      document.getElementById("cart").style.filter = "invert(0)";
      document.getElementById("cart").style.marginLeft = "auto";
      document.getElementById("cart").style.width = "10%";
    } else {
      document.getElementById("cart").style.filter = "invert(1)";
      document.getElementById("cart").style.marginLeft = "0";
      document.getElementById("cart").style.width = "40%";
    }
    if (searchQuery.toLocaleLowerCase() !== "") {
      setPage(1);
      setCategory("all");
      const timer = setTimeout(() => {
        if (!isGrid) {
          filterProductsByBrandQuery(category, searchQuery, page, pageSize)
            .then((response) => {
              setLoading(false);
              setDataInfo(response.data);
              setProductsByBrands(response.data.brands);
            })
            .catch((error) => {
              console.log(error);
            });
        }
      }, 500);
      return () => clearTimeout(timer);
    } else {
      filterProductsByBrandQuery(category, searchQuery, page, pageSize)
        .then((response) => {
          setProductsByBrands([...productsByBrands, ...response.data.brands]);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [searchQuery, allProducts?.products, category, page, isGrid]);
  const handleSearchBoxChange = (e) => {
    setSearchQuery(e.target.value);
  };
  const handleFilterClick = (selectedCategory) => {
    setPage(1);
    setCategory(selectedCategory);
    setSearchQuery("");
    setProductsByBrands([...[]]);
  };
  const handleChange = (event, value) => {
    setPage(value);
  };
  const onScroll = () => {
    if (listInnerRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
      if (scrollTop + clientHeight === scrollHeight) {
        setPage(page + 1);
      }
    }
  };
  return (
    <div className="product-wrapper" id="products">
      <Navbar />
      <Heading headingInfo={{ heading: "Products", title: "Home", subTitle: "Products" }} />
      <div className="container-fluid">
        <div className="search-container">
          <div className="col-12">
            <input type="text" className="form-control" placeholder="Search by brand or capacity or company" value={searchQuery} onChange={handleSearchBoxChange} />
          </div>
          <div className="col-12">
            <FilterButtons handleFilterClick={handleFilterClick} category={category} isGrid={isGrid}></FilterButtons>
          </div>
        </div>
        {loading && (
          <div className="box-spinner">
            <Grid height="80" width="80" color="#cf0a2c" ariaLabel="grid-loading" radius="12.5" wrapperStyle={{}} wrapperClass="" visible={true} />
          </div>
        )}
        {productsByBrands && !loading && <Products products={!isGrid ? productsByBrands : products} isGrid={isGrid} onScroll={onScroll} listInnerRef={listInnerRef} />}
        {productsByBrands?.length <= 0 && !loading && (
          <div className="empty-products">
            <i className="pi  pi-inbox" style={{ fontSize: "2rem" }}></i>
            <small>No Products Found</small>
          </div>
        )}
        {products?.length > 0 && isGrid && (
          <div className="box-pagination">
            <Pagination count={dataInfo?.totalPages} defaultValue={page} page={page} variant="outlined" color="secondary" shape="rounded" onChange={handleChange} />
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductsWrapper;
