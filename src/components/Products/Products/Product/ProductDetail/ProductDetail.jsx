/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import "./ProductDetail.scss";
import ProductFooter from "./ProductFooter/ProductFooter";
import { downloadDatasheet } from "../../../../../actions/productActions";
import { useDispatch, useSelector } from "react-redux";
import { addProductToCart, removeProductToCart } from "../../../../../store/cartSlice";
import { getProductById } from "../../../../../api";
import { isMobile } from "react-device-detect";
import HeaderProduct from "./HeaderProduct/HeaderProduct";
import ProductFeatures from "./ProductFeaures/ProductFeatures";
import Footer from "../../../../Footer/Footer";
const ProductDetail = () => {
  const dispatch = useDispatch();
  window.scrollTo({ top: 0, behavior: "smooth" });
  const { state } = useLocation();
  let { id } = useParams();
  let productsCart = useSelector((state) => state.cart.products);
  const [product, setProduct] = useState({});
  const [productImage, setProductImage] = useState("");
  const handleDatasheet = async (id, downloadedFileName) => {
    downloadDatasheet(id, downloadedFileName);
  };
  useEffect(() => {
    getProductById(id)
      .then((response) => {
        setProduct(response.data);
        setProductImage(response.data.image[0].image);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const addToCartHandler = (product) => {
    dispatch(addProductToCart(product));
  };
  const removeToCartHandler = (product) => {
    dispatch(removeProductToCart(product));
  };
  const navigate = useNavigate();
  return (
    <div>
      <div className="navbar-product">
        <div className="container-fluid position-relative p-0">
          <nav className="navbar navbar-expand-lg navbar-dark  py-3 py-lg-0">
            <Link to="/" className="navbar-brand p-0">
              <h1 className="m-0 ">
                <span className="icon-logo">AGS</span>{" "}
              </h1>
            </Link>
            <div className="cart-icon ">
              <img src="../img/Icons/shopping-cart.png" onClick={() => navigate("/cart")} alt="shopping-cart.png" />
              <span class="badge badge-danger">{productsCart?.length}</span>
            </div>
          </nav>
        </div>
      </div>
      <div className="product-details" style={{ backgroundImage: 'url("./img/product4.png")' }}>
        <div className="container">
          <HeaderProduct
            product={product}
            addToCartHandler={addToCartHandler}
            removeToCartHandler={removeToCartHandler}
            handleDatasheet={handleDatasheet}
            productImage={productImage}
            productsCart={productsCart}
          />
          <div className="container">
            <ProductFeatures product={product} />
            <ProductFooter product={product} />
          </div>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default ProductDetail;
