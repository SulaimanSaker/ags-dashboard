import React from "react";
import "./ProductFeatures.scss";
const ProductFeatures = ({ product }) => {
  return (
    <div>
      <div className="row features-product mb-4">
        {product?.info?.productFeatureOneTitle && (
          <div className="col-lg-3">
            <div className="box">
              <div className="product-content">
                <h6>{product?.info?.productFeatureOneTitle}</h6>
                <p>{product?.info?.productFeatureOneDescription}</p>
              </div>
            </div>
          </div>
        )}
        {product?.info?.productFeatureTwoTitle && (
          <div className="col-lg-3">
            <div className="box">
              <div className="product-content">
                <h6>{product?.info?.productFeatureTwoTitle}</h6>
                <p>{product?.info?.productFeatureTwoDescription}</p>
              </div>
            </div>
          </div>
        )}
        {product?.info?.productFeatureThreeTitle && (
          <div className="col-lg-3">
            <div className="box">
              <div className="product-content">
                <h6>{product?.info?.productFeatureThreeTitle}</h6>
                <p>{product?.info?.productFeatureThreeDescription}</p>
              </div>
            </div>
          </div>
        )}
        {product?.info?.productFeatureFourTitle && (
          <div className="col-lg-3">
            <div className="box">
              <div className="product-content">
                <h6>{product?.info?.productFeatureFourTitle}</h6>
                <p>{product?.info?.productFeatureFourDescription}</p>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductFeatures;
