/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./HeaderProduct.scss";
import { getWordsFromSentence } from "../../../../../../helper/getWordsFromSentence";
import { useNavigate } from "react-router-dom";
import ReactGA from "react-ga4";
const HeaderProduct = ({ product, addToCartHandler, handleDatasheet, productImage, removeToCartHandler, productsCart }) => {
  const navigate = useNavigate();
  let productsCount = productsCart.filter((p) => p._id === product._id).length;
  const DummyText = (
    <div>
      <span>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        <span className="required"> (coming soon) </span>
      </span>
    </div>
  );
  const getPrice = (product) => {
    return (product?.productPrice * product?.usdRate + product?.productPriceMargin).toFixed(2);
  };
  return (
    <div>
      <div className="row">
        <div className="col-lg-4">
          <div class="single-product-info-left">
            <div class="content-info">
              <span class="sub-title" onClick={() => navigate("/products")}>
                <i class="fas fa-long-arrow-alt-left"></i>
              </span>
              <h1 class="title wow fadeInUp">
                {product?.brand}
                <span className="break-line">
                  {" "}
                  <br />
                </span>{" "}
                <span class="text-primary">{product?.capacity}</span>
                <span className="break-line">
                  <br />
                </span>{" "}
                {product?.code} <br />
              </h1>
              <p class="text">{product?.info?.productDescription !== " " ? getWordsFromSentence(product?.info?.productDescription, 50) : DummyText}</p>
              <div class="swiper-meta-items">
                <div class="meta-content">
                  <span class="price-name">Price</span>
                  {product?.info?.productPrice > 0 && <h2 class="price-num">{getPrice(product.info)} AED</h2>}
                  {product?.info?.productPrice <= 0 && <span class="price-num"> Out of Stock</span>}
                </div>
                {product?.info?.productPrice > 0 && (
                  <div class="meta-content ">
                    <span class="qty-name">Quantity</span>
                    <div class="d-flex align-items-center color-filter">
                      <span className="capacity-num" onClick={() => removeToCartHandler(product)}>
                        <i class="uil uil-minus"></i>
                      </span>
                      <span className="qty-field">
                        <input type="text" value={productsCount} disabled class="form-control " />
                      </span>
                      <span className="capacity-num" onClick={() => addToCartHandler(product)}>
                        <i class="uil uil-plus"></i>
                      </span>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div class="single-product-media">
            <img src={productImage} alt="/" />
          </div>
        </div>
        <div className="col-lg-4">
          <div class="single-product-info-right">
            <h3 class="dz-title ">Safety Certified</h3>
            <ul class="service-icon-bx">
              <li class="info-box ">
                <a href="" class="nav-link">
                  <div class="dz-icon">
                    <i class="uil uil-shield"></i>
                  </div>
                  <div class="info-content">
                    <span>Warranty </span>
                    <h4 class="title mb-0">{product?.info?.productProperties[1]?.value || 0} Yrs</h4>
                  </div>
                </a>
              </li>
              <li class="info-box ">
                <a href="" class="nav-link">
                  <div class="dz-icon">
                    <i class="uil uil-weight"></i>
                  </div>
                  <div class="info-content">
                    <span>Weight Capacity</span>
                    <h4 class="title mb-0">{product?.info?.productProperties[0]?.value || 0} Kgs</h4>
                  </div>
                </a>
              </li>
            </ul>
            <ul class="service-icon-list">
              <li class="wow fadeInUp">
                <img src="../img/Icons/colorkit.svg" alt="/" />
                <h3 class="title">{product?.info?.productSafetyFeatureOne || "High Quality"}</h3>
              </li>
              <li class="wow fadeInUp">
                <img src="../img/Icons/colorkit.svg" alt="/" />
                <h3 class="title">{product?.info?.productSafetyFeatureTwo || "Renewable Power Solution"}</h3>
              </li>
              <li class="wow fadeInUp">
                <img src="../img/Icons/colorkit.svg" alt="/" />
                <h3 class="title"> {product?.info?.productSafetyFeatureThree || "Sustainable Energy Source"} </h3>
              </li>
            </ul>
            <div class="product-video-info ">
              <div class="dz-info">
                <div className="btn-wrapper">
                  <div className="marketing-icons">
                    <div
                      className="icon-img"
                      onClick={() =>
                        product?.info?.sharafdgLink !== " "
                          ? window.open(product?.info?.sharafdgLink, "_blank")
                          : window.open("https://www.amazon.ae/s?me=A1TU1VGYM7FUEN&marketplaceID=A2VIGQ35RCS4UG", "_blank")
                      }
                    >
                      <img src="../img/Icons/Sharafdg1.jpg" className="sharaf-dg" alt="sharaf" />
                    </div>
                    <div
                      className="icon-img"
                      onClick={() =>
                        product?.info?.noonLink !== " "
                          ? window.open(product?.info?.noonLink, "_blank")
                          : window.open("https://www.noon.com/seller/p-73808/?offer_code=z49dccd79bbf30885ec0cz-1&sku=Z49DCCD79BBF30885EC0CZ")
                      }
                    >
                      <img src="../img/Icons/noon1.png" className="noon-img" alt="" />
                    </div>
                    <div
                      className="icon-img"
                      onClick={() =>
                        product?.info?.amazonLink !== " "
                          ? window.open(product?.info?.amazonLink, "_blank")
                          : window.open("https://www.amazon.ae/s?me=A1TU1VGYM7FUEN&marketplaceID=A2VIGQ35RCS4UG", "_blank")
                      }
                    >
                      <img src="../img/Icons/amazon1.jpg" className="amazon-img" alt="" />
                    </div>
                    <div
                      className="icon-img"
                      onClick={() =>
                        product?.info?.mircrolessLink !== " "
                          ? window.open(product?.info?.mircrolessLink, "_blank")
                          : window.open("https://uae.microless.com/seller/abduljalil/", "_blank")
                      }
                    >
                      <img src="../img/Icons/micorless1.jpg" className="mircoless-img" alt="" />
                    </div>
                    <div
                      className="icon-img"
                      onClick={() => window.open("https://api.whatsapp.com/send/?phone=971542899793&text=Hello+AGS+i+need+your+help.&type=phone_number&app_absent=0", "_blank")}
                    >
                      <img src="../img/Icons/whatsapp.png" className="whatsapp-img" alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-btn wow fadeInUp" data-wow-delay="0.6s" data-swiper-parallax="-60">
              <a
                class="btn btn-outline-secondary"
                onClick={() => {
                  handleDatasheet(product?._id, product?.code);
                  ReactGA.event({
                    category: "Button",
                    action: "Click",
                    label: `Datasheet ${product?.code}  Downloaded`,
                    value: 101,
                  });
                }}
              >
                <span>Datasheet</span>
                <i class="uil uil-import icon-download"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderProduct;
