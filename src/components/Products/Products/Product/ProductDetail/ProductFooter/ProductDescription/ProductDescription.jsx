import React from "react";
import "./ProductDescription.scss";
const ProductDescription = ({ product }) => {
  const DummyText = (
    <div>
      <span>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
        <span className="required"> (coming soon) </span>
      </span>
    </div>
  );
  return (
    <div>
      <div className="item-feature">
        <h4>Introduction</h4>
        <p>{product?.info?.productIntroduction !== " " ? product?.info?.productIntroduction : DummyText}</p>
      </div>
      <div className="item-feature">
        <h4>Specifications</h4>
        <p>{product?.info?.productSpecifications !== " " ? product?.info?.productSpecifications : DummyText}</p>
      </div>
      <div className="item-feature">
        <h4>Feature</h4>
        <p>{product?.info?.productFeature !== " " ? product?.info?.productSpecifications : DummyText}</p>
      </div>
    </div>
  );
};

export default ProductDescription;
