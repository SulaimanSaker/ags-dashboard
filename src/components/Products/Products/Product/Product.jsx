import React, { useState } from "react";
import "./Product.scss";
import { useNavigate } from "react-router-dom";
import { getWordsFromSentence } from "../../../../helper/getWordsFromSentence";
import ReactGA from "react-ga4";

const Product = ({ product, isGrid }) => {
  let [index, setIndex] = useState(0);
  const navigate = useNavigate();
  const getPrice = (product) => {
    return (product?.productPrice * product?.usdRate + product?.productPriceMargin).toFixed(2);
  };
  const DummyText = (
    <div>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        <span className="required"> (coming soon) </span>
      </p>
    </div>
  );
  return (
    <>
      {!isGrid && (
        <div className="product-item-card">
          <div className="left-section">
            <div className="product-image">
              <img src={product?.products[index]?.image[0]?.image} alt="product-img" />
            </div>
            <div className="product-capacity">
              <p class="pick">Choose Capacity</p>
              <div class="sizes">
                {product.products.map((product, indexProduct) => (
                  <div
                    class={`size  ${indexProduct === index ? "selected-capacity" : ""} ${product?.LocalPrice <= 0 ? "non-of-stock" : ""} `}
                    key={indexProduct}
                    onClick={() => setIndex(indexProduct)}
                  >
                    {product.capacity}
                  </div>
                ))}
              </div>
            </div>
            <span className="icon-brand">{product.brand}</span>
          </div>
          <div className="right-section">
            <div className="product-info">
              <div class="product">
                <div>
                  <p>{product?.products[index]?.category}</p>
                  <h1>
                    {product?.products[index]?.company} {product?.products[index]?.code} {product?.products[index]?.brand}
                  </h1>
                  <h2 className="price ">
                    <span className="required">{product?.products[index]?.capacity}</span>
                    {product?.products[index]?.info?.productPrice > 0 && <span>{getPrice(product?.products[index]?.info)} AED</span>}
                    <small> {product?.products[index]?.info?.productPrice <= 0 && <span>Only WholeSale</span>}</small>
                  </h2>
                  <p class="desc">
                    {product?.products[index]?.info?.productDescription !== " " ? getWordsFromSentence(product?.products[index]?.info?.productDescription, 30) + "..." : DummyText}
                  </p>
                </div>
                <div class="buttons">
                  <button
                    class="add"
                    onClick={() => {
                      navigate(`/product-detail/${product?.products[index]?._id}`, { state: product });
                      // Send a click event to Google Analytics
                      ReactGA.event({
                        category: "Button",
                        action: "Click",
                        label: `${product?.products[index]?.code} Viewed`,
                        value: 101,
                      });
                    }}
                  >
                    View Details
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      {isGrid && (
        <div className="product" data-wow-delay="0.3s">
          <div className="card">
            <div className="imgBox">
              <img src={product?.image[0]} className="img-product" alt={`${product.category}-alt`} />
            </div>
            <div className="contentBox">
              <h4 className="heading-product"> {product.brand}</h4>
              <h6 style={{ fontSize: "16px", fontFamily: "monospace", fontWeight: "600", color: "rgb(197 19 19)" }}> {product.code}</h6>
              <h5 className="price">{product.capacity}</h5>
              {product?.info?.productPrice > 0 && <h5 className="price required ">{getPrice(product.info)} AED</h5>}
              {product?.info?.productPrice <= 0 && <small className="required"> Out of Stock</small>}
              <div className="button_container">
                <button className="ags-btn-main-fill" onClick={() => navigate(`/product-detail/${product._id}`, { state: product })}>
                  {" "}
                  See More
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Product;
