import React from "react";
import "./Products.scss";
import Product from "./Product/Product";
const Products = ({ products, isGrid, onScroll, listInnerRef }) => {
  return (
    <>
      {!isGrid && (
        <div className="all-products-wrapper">
          <div className="container-fluid">
            <div className="row " onScroll={onScroll} ref={listInnerRef} style={{ height: products.length >= 6 ? "100vh" : "100%", overflowY: "scroll", scrollbarWidth: "none" }}>
              {products?.map((product, index) => (
                <div key={index} className="col-lg-4">
                  <Product product={product} key={index} isGrid={isGrid} />
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
      {isGrid && (
        <div className="products-grid mt-4" data-wow-delay="0.1s" id="team">
          {products?.map((product, index) => (
            <Product product={product} key={index} isGrid={isGrid} />
          ))}
        </div>
      )}
    </>
  );
};

export default Products;
