import React from "react";
import "./NewsBlog.scss";
import OwlCarousel from "react-owl-carousel";
import { useNavigate } from "react-router-dom";
const NewsBlog = () => {
  const navigate = useNavigate();
  const options = {
    loop: true,
    margin: 0,
    center: true,
    autoplaySpeed: 3000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  };
  return (
    <div className="container-fluid pt-3  wow fadeInUp blogs" id="blogsNews" data-wow-delay="0.1s">
      <div className="container ">
        <div className="section-title text-center position-relative pb-3 mb-4 mx-auto">
          <h5 className="fw-bold text-primary text-uppercase">Latest News</h5>
          <h1 className="mb-0">Read The Latest News </h1>
        </div>
        <OwlCarousel {...options} className="owl-theme">
          <div className="container">
            <div className="wrapper_news">
              <div className="blog_news">
                <div className="blog_img">
                  <img src="https://source.unsplash.com/600x400/?solar" alt="" />
                </div>
                <div className="blog_info">
                  <div className="blog_title">
                    <h1>Green Energy Industry</h1>
                  </div>
                  <div className="blog_date">
                    <div className="d-flex mb-3">
                      <small>
                        <i className="far fa-calendar-alt text-primary me-2"></i>Sunday, October 5 2023
                      </small>
                    </div>
                  </div>
                  <div className="blog_text">
                    Visit us at our booth for updates on the latest developments in the Green energy industry. Make sure to catch our offers and talk to our representatives as they
                    will be very delighted to help with all your inquiries.
                  </div>
                  <a onClick={() => navigate("/news")} className="blog_btn_more c_pointer">
                    Read more
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
              <div className="wrapper_news">
                <div className="blog_news">
                  <div className="blog_img">
                    <img src="https://source.unsplash.com/600x400/?solar" alt="" />
                  </div>
                  <div className="blog_info">
                    <div className="blog_title">
                      <h1>Green Energy Industry</h1>
                    </div>
                    <div className="blog_date">
                      <div className="d-flex mb-3">
                        <small>
                          <i className="far fa-calendar-alt text-primary me-2"></i>Sunday, October 5 2023
                        </small>
                      </div>
                    </div>
                    <div className="blog_text">
                      Visit us at our booth for updates on the latest developments in the Green energy industry. Make sure to catch our offers and talk to our representatives as
                      they will be very delighted to help with all your inquiries.
                    </div>
                    <a onClick={() => navigate("/news")} className="blog_btn_more c_pointer">
                      Read more
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
              <div className="wrapper_news">
                <div className="blog_news">
                  <div className="blog_img">
                    <img src="https://source.unsplash.com/600x400/?solar" alt="" />
                  </div>
                  <div className="blog_info">
                    <div className="blog_title">
                      <h1>Green Energy Industry</h1>
                    </div>
                    <div className="blog_date">
                      <div className="d-flex mb-3">
                        <small>
                          <i className="far fa-calendar-alt text-primary me-2"></i>Sunday, October 5 2023
                        </small>
                      </div>
                    </div>
                    <div className="blog_text">
                      Visit us at our booth for updates on the latest developments in the Green energy industry. Make sure to catch our offers and talk to our representatives as
                      they will be very delighted to help with all your inquiries.
                    </div>
                    <a onClick={() => navigate("/news")} className="blog_btn_more c_pointer">
                      Read more
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </OwlCarousel>
      </div>
    </div>
    // </div>
  );
};

export default NewsBlog;
