import React from "react";
import "./Facts.scss";
import { FactsItems } from "../../Data/Data";
import CountUp from "react-countup";
const Facts = () => {
  return (
    <div>
      <div className="container-fluid facts  pt-lg-0">
        <div className="container py-5 pt-lg-0">
          <div className="row gx-0">
            {FactsItems.map((fact, index) => (
              <div key={index} className="col-lg-4 wow zoomIn" data-wow-delay={fact.delay}>
                <div className={` ${fact.classNames}`}>
                  <div className={` ${fact.classNameIcon}`}>
                    <i className={`${fact.icon}`}></i>
                  </div>
                  <div className="ps-4">
                    <h5 className={fact.classNameContent}>{fact.title}</h5>
                    <h1 className={fact.classNameContent} data-toggle="counter-up">
                      <CountUp end={fact.description} duration={5}></CountUp> {fact.isPlus ? "+" : ""}
                    </h1>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Facts;
