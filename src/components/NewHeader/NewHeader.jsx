import React from "react";
import "./NewHeader.scss";
const NewHeader = () => {
  return (
    <div className="header-page">
      <div className="main">
        <section className="hero-section hero-section-3 ptb-100">
          <div className="circles">
            <div className="point animated-point-1" />
            <div className="point animated-point-2" />
            <div className="point animated-point-3" />
            <div className="point animated-point-4" />
            <div className="point animated-point-5" />
            <div className="point animated-point-6" />
            <div className="point animated-point-7" />
            <div className="point animated-point-8" />
            <div className="point animated-point-9" />
          </div>
          <div className="container">
            <div className="row row-header">
              <div className="col-md-6 col-lg-6 wow rollIn" data-wow-delay="0.1s">
                <div className="hero-content-left pt-5">
                  <h1>
                    We sell <span>Best quality </span> <br /> Products
                  </h1>
                  <p className="lead">
                    Explore our wide range of high-quality solar panels and efficient batteries, tailored for your energy needs and environmental consciousness.
                  </p>
                  <ul className="list-inline  core-feature-list d-flex">
                    <li className="list-inline-item text-center">
                      <a
                        className="icon-box mb-2 mx-auto primary-bg wow rotateInDownLeft"
                        data-wow-delay=".5s"
                        href="https://goo.gl/maps/D61uNbU9abQZ6CPZ6"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <i class="uil uil-map-marker"></i>
                      </a>
                    </li>
                    <li className="list-inline-item text-center">
                      <a className="icon-box mb-2 mx-auto primary-bg wow rotateInDownLeft" data-wow-delay=".8s" href="tel:+971542899793" rel="noreferrer">
                        <i class="uil uil-phone"></i>
                      </a>
                    </li>
                    <li className="list-inline-item text-center">
                      <a className="icon-box mb-2 mx-auto primary-bg wow rotateInDownLeft" data-wow-delay="1.1s" href="mailTo:info@ags.ac" target="_blank" rel="noreferrer">
                        <i class="uil uil-envelope"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 col-lg-6">
                <div className="hero-animation-img pt-lg-4 wow rotateInDownRight" data-wow-delay="0.3s">
                  <img className="img-fluid d-block m-auto animation-one  " src="img/solar5.png" alt="animation-image" />
                  <img className="img-fluid d-none d-lg-block animation-three " src="img/luminouse.png" alt="animation image" width={280} />
                </div>
              </div>
            </div>
          </div>
          <img src="img/download.svg" className="shape-image" alt="shape image" />
        </section>
      </div>
    </div>
  );
};

export default NewHeader;
