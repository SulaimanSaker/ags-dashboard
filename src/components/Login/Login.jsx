import React, { useEffect, useState } from "react";
import Heading from "../Heading/Heading";
import "./Login.scss";
import { useNavigate } from "react-router-dom";
import { isMobile } from "react-device-detect";
const Login = () => {
  const navigate = useNavigate();
  const [fromData, setFormData] = useState({
    username: "",
    password: "",
  });
  const login = (e) => {
    if (fromData.username === "Admin" && fromData.password === "admin@ags") {
      localStorage.setItem("username", "Admin");
      localStorage.setItem("password", "admin@ags");
      navigate("/admin-ags-793/products");
    }
  };
  const handleInputChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    setFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  window.scrollTo({ top: 0, behavior: "smooth" });
  return (
    <>
      <Heading headingInfo={{ heading: "Login", title: "Home", subTitle: "Login" }} />
      <div className="login-container">
        <div className="container">
          <input type="checkbox" id="flip" />
          <div className="cover">
            <div className="front">
              <img src="./img/header_8.jpg" alt="" />
              <div className="text">
                <span className="text-1">
                  Welcome back! <br />
                </span>
                <span className="text-2">AGS International Power Solution</span>
              </div>
            </div>
            <div className="back">
              <img className="backImg" src="./img/header_8.jpg" alt="" />
              <div className="text">
                <span className="text-1">
                  Complete miles of journey <br /> with one step
                </span>
                <span className="text-2">Let's get started</span>
              </div>
            </div>
          </div>
          <div className="forms">
            <div className="form-content">
              <div className="login-form">
                <div className="title">Login</div>
                <form onSubmit={login}>
                  <div className="input-boxes">
                    <div className="input-box">
                      <i className="fas fa-envelope"></i>
                      <input
                        type="text"
                        placeholder="Enter your email"
                        name="username"
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="input-box">
                      <i className="fas fa-lock"></i>
                      <input
                        type="password"
                        placeholder="Enter your password"
                        name="password"
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="text">
                      <a href="#">Forgot password?</a>
                    </div>
                    <div className="button input-box">
                      <input type="submit" value="Log in" />
                    </div>
                    <div className="text sign-up-text"></div>
                  </div>
                </form>
              </div>
              <div className="signup-form">
                <div className="title">Signup</div>
                <form action="#">
                  <div className="input-boxes">
                    <div className="input-box">
                      <i className="fas fa-user"></i>
                      <input type="text" placeholder="Enter your name" required />
                    </div>
                    <div className="input-box">
                      <i className="fas fa-envelope"></i>
                      <input type="text" placeholder="Enter your email" required />
                    </div>
                    <div className="input-box">
                      <i className="fas fa-lock"></i>
                      <input type="password" placeholder="Enter your password" required />
                    </div>
                    <div className="button input-box">
                      <input type="submit" value="Sumbit" />
                    </div>
                    <div className="text sign-up-text">
                      Already have an account? <label htmlFor="flip">Login now</label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
