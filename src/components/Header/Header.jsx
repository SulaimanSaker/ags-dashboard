import React from "react";
import "./Header.scss";
import Navbar from "../Navbar/Navbar";
import Carousel from "../Carousel/Carousel";
const Header = () => {
  return (
    <div className="container-fluid position-relative p-0">
      <Carousel />
    </div>
  );
};

export default Header;
