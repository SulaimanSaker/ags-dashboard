import React from "react";
import "./CarouselOffer.scss";
import OwlCarousel from "react-owl-carousel";
import { useNavigate } from "react-router-dom";
const CarouselOffer = ({ products }) => {
  const navigate = useNavigate();
  const options = {
    loop: true,
    margin: 20,
    nav: false,
    dots: false,
    center: true,
    autoplaySpeed: 5000,
    autoplayTimeout: 5000,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 2,
      },
    },
  };
  return (
    <>
      {products?.length > 0 && (
        <div className="offers-products">
          <div className="container-fluid">
            <OwlCarousel {...options} className="owl-theme ">
              {products.slice(8, 12).map((product, index) => (
                <div key={index} class="blog-slider__item swiper-slide" onClick={() => navigate(`/product-detail/${product._id}`, { state: product })}>
                  <div class="blog-slider__img">
                    <img src={product.image[0]} alt="" />
                  </div>
                  <div class="blog-slider__content">
                    <div class="blog-slider__title">
                      {product.brand} {product.code}
                      {product.capacity}
                    </div>
                    <div class="blog-slider__text">
                      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus sunt eveniet dicta quo Doloribus sunt eveniet dicta quo,
                    </div>
                  </div>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>
      )}
    </>
  );
};

export default CarouselOffer;
