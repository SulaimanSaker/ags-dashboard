import React from "react";
import "./NewsBar.scss";
const NewsBar = () => {
  return (
    <div className="marquee">
      <p>
        <i>Join us at The Solar Show KSA 2023, Oct 30 - 31 , 2023, Location Riyadh, Saudi Arabia</i>
      </p>
    </div>
  );
};

export default NewsBar;
