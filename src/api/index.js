import axios from "axios";
// export const BASE_URL = "https://fakestoreapi.com/";
export const BASE_URL = "https://www.ags.ac/api";
// export const BASE_URL = "http://10.255.254.31:5001";
// export const BASE_URL = "https://agints.vip/api";
// export const BASE_DOMAIN = "https://agints.vip";

const API = axios.create({ baseURL: BASE_URL });

// Route Products routes
export const fetchProducts = (page, pageSize) =>
  API.get(`/product?page=${page}&pageSize=${pageSize}`);
export const fetchProductsByBrand = () => API.get(`/product/brand`);
export const updateInfoProduct = (id, model) =>
  API.patch(`/product/info/${id}`, { updateData: model });
export const getProductById = (id) => API.get(`/product/${id}`);
export const filterProducts = (category, searchQuery, page, pageSize) =>
  API.get(
    `/product/search?category=${category}&searchQuery=${searchQuery}&page=${page}&pageSize=${pageSize}`
  );
export const filterProductsByBrands = (category, searchQuery, page, pageSize) =>
  API.get(
    `/product/brand?category=${category}&searchQuery=${searchQuery}&page=${page}&pageSize=${pageSize}`
  );
export const filterProductsByBrandQuery = (category, searchQuery, page, pageSize) =>
  API.get(
    `/product/brand?category=${category}&searchQuery=${searchQuery}&page=${page}&pageSize=${pageSize}`
  );

// Route Blog routes
export const getBlogs = (model) => API.get(`/blog`);
export const getBlogById = (id) => API.get(`/blog/${id}`);
export const addBlog = (model) => API.post(`/blog`, model);
export const updateBlog = (id, model) => API.put(`/blog/${id}`, model);
export const deleteBlog = (id) => API.delete(`/blog/${id}`);

// Route Service routes
export const getServices = (model) => API.get(`/service`);
export const getServiceById = (id) => API.get(`/service/${id}`);
export const addService = (model) => API.post(`/service`, model);
export const updateService = (id, model) => API.put(`/service/${id}`, model);
export const deleteService = (id) => API.delete(`/service/${id}`);

//this is for message form
export const getMessages = () => API.get(`/mail`);
export const addMessage = (model) => API.post(`/mail`, model);

// Cart APIS
export const addOrder = (model) => API.post(`/quote`, model);

// Admin APIS
export const updateProductVisibility = (id, visibility) =>
  API.patch(`/product/info/${id}?visibility=${visibility}`);
export const updateProductPriority = (id, brand, priority) =>
  API.patch(`/product/info/${id}?brand=${brand}&priority=${priority}`);

// cart APIS
export const getQuotes = () => API.get(`/quote`);
